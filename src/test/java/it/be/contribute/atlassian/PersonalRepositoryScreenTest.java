package it.be.contribute.atlassian;


import be.contribute.atlassian.pageobjects.TagListRepositoryNavigationWrapper;
import be.contribute.atlassian.pageobjects.page.PersonalCommitListPage;
import be.contribute.atlassian.pageobjects.page.tag.CreateTagPage;
import be.contribute.atlassian.pageobjects.page.tag.TagListPage;
import org.junit.Test;

public class PersonalRepositoryScreenTest extends AbstractTagScreenTest {
    private static final String PERSONAL_REPO_SLUG = "personal";
    private static final String USER = "admin";
    private static final String COMMIT_ID = "743b1db16d51ef00ae5b6bb118909f2d99fed136";


    @Test
    public void can_create_tag() throws InterruptedException {
        CreateTagPage createTagPage = BITBUCKET.visit(CreateTagPage.class, USER, PERSONAL_REPO_SLUG, COMMIT_ID, true);
        createTagPage.setTagName("personal_tag");
        createTagPage.setDescription("desrciption for my personal tag");
        createTagPage.submitCorrectInputAndRedirect(TagListPage.class, USER, PERSONAL_REPO_SLUG, true);
    }

    @Test
    public void can_view_tag_page() {
        PersonalCommitListPage commitListPage = BITBUCKET.visit(PersonalCommitListPage.class, USER, PERSONAL_REPO_SLUG);
        TagListRepositoryNavigationWrapper wrapper = new TagListRepositoryNavigationWrapper(commitListPage, getPageBinder(), true);
        TagListPage listPage = wrapper.goToTagListPage();
        listPage.isHere();
    }


}
