package ut.be.contribute.atlassian.servlet;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.servlet.AbstractTagServlet;
import be.contribute.atlassian.servlet.ViewTagServlet;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactory;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.UserService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageImpl;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ut.be.contribute.atlassian.tag.rest.TestFunction;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ViewTagServletTest extends AbstractTagServletTest {

    @Mock
    private TagService tagService;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;
    @Mock
    private TagRestResourceTransformationFactory tagRestResourceTransformationFactory;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationUser bitbucketUser;
    @Mock
    private PermissionService permissionService;

    private ViewTagServlet viewTagServlet;
    private ArrayList<DetailedTag> tagList ;
    private PageRequestImpl pageRequest;

    @Mock
    private DetailedTagRestResource restResource;
    @Mock
    private DetailedTag detailedTag;

    private PageImpl<DetailedTag> detailedTagPage;


    @Before
    public void setUp() {

        tagList = Lists.newArrayList(detailedTag);

        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);

        when(getRequest.getParameter("repo")).thenReturn(repoSlug);
        when(getRequest.getParameter("projKey")).thenReturn(PROJECT_KEY);
        pageRequest = new PageRequestImpl(0, 20);

        detailedTagPage = new PageImpl<DetailedTag>(pageRequest, tagList, true);
        when(tagService.getTags(any(Repository.class), any(PageRequest.class))).thenReturn(detailedTagPage);
        when(tagRestResourceTransformationFactory.getTransformation(any(Repository.class))).thenReturn(new TestFunction(restResource));

        viewTagServlet = new ViewTagServlet(soyTemplateRenderer, repositoryService,repositoryResolver, tagService, pageBuilderService, tagRestResourceTransformationFactory, applicationPropertiesService, permissionService);
        when(userService.getUserBySlug(PROJECT_KEY)).thenReturn(bitbucketUser);
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(true);
    }

    @Test
    public void doGet_rendersCorrectTemplate() throws IOException, ServletException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
        assertCorrectTemplate("Contribute.Stash.Templates.TagList.tagOverview");
    }

    @Test
    public void doGet_hasCorrectRepositoryParam() throws ServletException, IOException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("repository", repository);
    }

    @Test
    public void doGet_setsDefaultStartingFrom() throws ServletException, IOException {
        when(getRequest.getParameter("startingFrom")).thenReturn(null);

        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();

        hasParam("startingFrom", 0);
    }

    @Test
    public void doGet_setsCorrectStartingFrom() throws ServletException, IOException {
        int startingFrom = new Random().nextInt(15);
        when(getRequest.getParameter("startingFrom")).thenReturn(startingFrom + "");

        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();

        hasParam("startingFrom", startingFrom);
    }

    @Test
    public void doGet_getsAddsViewableTagsToResponse() throws ServletException, IOException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();

        Page<DetailedTagRestResource> detailedTagRestResourcePage = (Page<DetailedTagRestResource>) getParam("tags");
        assertThat(detailedTagRestResourcePage.getValues()).contains(restResource);
    }

    @Test
    public void doGet_addsWebresourcesForContext() throws ServletException, IOException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
       verify(requiredResources).requireContext("contribute.page.taglist");
    }

    @Test
    public void doGet_personalRepository() throws ServletException, IOException {
        when(getRequest.getParameter("personal")).thenReturn("true");
        viewTagServlet.doGetTestable(getRequest, getResponse);
    }

    @Test
    public void doGet_setsBaseUrl() throws ServletException, IOException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("baseUrl", BASE_URL);
    }

    @Test
    public void doGet_addsPermissionAllowed() throws ServletException, IOException {
        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("permission", true);
    }

    @Test
    public void doGet_addsPermissionNotAllowed() throws ServletException, IOException {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        viewTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("permission", false);
    }





    @Override
    protected AbstractTagServlet getServlet() {
        return viewTagServlet;
    }


}
