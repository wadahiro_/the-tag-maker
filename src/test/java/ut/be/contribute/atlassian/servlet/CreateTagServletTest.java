package ut.be.contribute.atlassian.servlet;

import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.service.ViewableManager;
import be.contribute.atlassian.servlet.AbstractTagServlet;
import be.contribute.atlassian.servlet.CreateTagServlet;
import be.contribute.atlassian.validator.CreateTagFormValidator;
import be.contribute.atlassian.web.ChangesetViewable;
import be.contribute.atlassian.web.CreateTagFormResult;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CreateTagServletTest extends AbstractTagServletTest {

    private static final String COMMIT_ID = "a3b576dv";
    private static final String TAG_NAME = "v1.1.1";
    private static final String DESCRIPTION = "First version since blah balh";
    private static final String FROM_PAGE = BASE_URL + "/LastPage";
    private static final String NOT_ALLOWED_MSG = "Sorry mate. You're not allowed to create tags in this repository";
    private CreateTagServlet createTagServlet;

    @Mock
    private CommitService commitService;
    @Mock
    private Commit changeset;
    @Mock
    private ViewableManager viewableManager;
    @Mock
    private TagService tagService;

    @Mock
    private CreateTagFormValidator createTagFormValidator;
    @Mock
    private PermissionService permissionService;


    private ChangesetViewable commitViewable;

    @Before
    public void setUp() throws URISyntaxException {
        commitViewable = new ChangesetViewable();

        when(postRequest.getParameter("repoId")).thenReturn(repoId + "");
        when(postRequest.getParameter("commitId")).thenReturn(COMMIT_ID);
        when(postRequest.getParameter("tagName")).thenReturn(TAG_NAME);
        when(postRequest.getParameter("description")).thenReturn(DESCRIPTION);
        when(postRequest.getParameter("lastPage")).thenReturn(FROM_PAGE);


        when(getRequest.getParameter("commitId")).thenReturn(COMMIT_ID);
        when(getRequest.getParameter("repoId")).thenReturn(repoId + "");
        when(getRequest.getHeader("Referer")).thenReturn(FROM_PAGE);


        when(commitService.getCommit(new CommitRequest.Builder(repository, COMMIT_ID).build())).thenReturn(changeset);

        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(true);

        when(viewableManager.createViewAble(changeset, repository)).thenReturn(commitViewable);
        createTagServlet = new CreateTagServlet(soyTemplateRenderer, repositoryService, repositoryResolver, commitService, createTagFormValidator, applicationPropertiesService, viewableManager, tagService, permissionService);
    }




    @Test
    public void doGet_rendersCorrectTemplate() throws IOException, ServletException {
        createTagServlet.doGetTestable(getRequest, getResponse);
        assertCorrectTemplate("Contribute.Stash.Templates.CreateTag.createTagView");
    }

    @Test
    public void doGet_hasCorrectRepoParam() throws ServletException, IOException {
        createTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("repository", repository);
        CreateTagFormResult formResult = getCreateTagFormResult();
        assertThat(formResult).isNotNull();
    }

    @Test
    public void doPostIncorrect_hasCorrectRepoParam() throws ServletException, IOException {
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);
        captureParams();
        hasParam("repository", repository);
        CreateTagFormResult formResult = getCreateTagFormResult();
        assertThat(formResult).isNotNull();
    }

    @Test
    public void doGet_hasCorrectCommitViewParam() throws ServletException, IOException {
        createTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        hasParam("commitViewable", commitViewable);
        CreateTagFormResult formResult = getCreateTagFormResult();
        assertThat(formResult).isNotNull();
    }

    @Test
    public void doPostIncorrect_hasCorrectChangeViewSetParam() throws ServletException, IOException {
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);
        captureParams();
        hasParam("commitViewable", commitViewable);
        CreateTagFormResult formResult = getCreateTagFormResult();
        assertThat(formResult).isNotNull();
    }

    @Test
    public void doPost_createsNewTag() throws ServletException, IOException {
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(tagService).createTag(TAG_NAME, DESCRIPTION, COMMIT_ID, repository);
    }

    @Test
    public void doPost_validatesForm() throws ServletException, IOException {
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(createTagFormValidator).validate(any(CreateTagFormResult.class));
    }

    @Test
    public void doPost_notCreatesTagOnError() throws ServletException, IOException {
        when(postRequest.getParameter("tagName")).thenReturn(null);
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);

        verify(tagService, never()).createTag(TAG_NAME, DESCRIPTION, COMMIT_ID, repository);
    }

    @Test
    public void doPost_returnsToFormOnError() throws ServletException, IOException {
        when(postRequest.getParameter("tagName")).thenReturn(null);
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);

        assertCorrectTemplate("Contribute.Stash.Templates.CreateTag.createTagView");

    }


    @Test
    public void doPost_setFormResultOnParams() throws ServletException, IOException {
        when(postRequest.getParameter("tagName")).thenReturn(null);
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);
        captureParams();

        CreateTagFormResult createTagFormResult = getCreateTagFormResult();
        assertThat(createTagFormResult.hasErrors()).isTrue();
    }

    @Test
    public void doGet_worksAlsoWithRepoSlug() throws ServletException, IOException {
        when(getRequest.getParameter("repoId")).thenReturn(null);
        when(getRequest.getParameter("repoSlug")).thenReturn(repoSlug);
        when(getRequest.getParameter("projectKey")).thenReturn(PROJECT_KEY);
        createTagServlet.doGetTestable(getRequest, getResponse);
        verify(repositoryResolver).resolveRepository(PROJECT_KEY, repoSlug, false);
    }

    @Test
    public void doGet_setCorrectRedirectPageInParams() throws ServletException, IOException {
        createTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        assertThat(getParam("lastPage")).isEqualTo(FROM_PAGE);
    }

    @Test
    public void doPost_setLastPageOnIncorrectResult() throws ServletException, IOException {
        when(postRequest.getParameter("tagName")).thenReturn(null);
        invalidResult();
        createTagServlet.doPostTestable(postRequest, postResponse);
        captureParams();

        assertThat(getParam("lastPage")).isEqualTo(FROM_PAGE);
    }

    @Test
    public void doPost_returnsToLastPageWhenCorrect() throws ServletException, IOException {
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(postResponse).sendRedirect(FROM_PAGE);
    }

    @Test
    public void doPost_returnsToLastPageWhenCorrect_caseInsensitiveBaseUrl() throws ServletException, IOException, URISyntaxException {
        when(applicationPropertiesService.getBaseUrl()).thenReturn(new URI(BASE_URL.toUpperCase()));
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(postResponse).sendRedirect(FROM_PAGE);
    }

    @Test
    public void doPost_sendsToDefaultWhenNoBitbucketURl() throws ServletException, IOException {
        when(postRequest.getParameter("lastPage")).thenReturn("incorrectPath");
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(postResponse).sendRedirect(getDefaultPage());
    }

    @Test
    public void doGet_fixesRedirectPageInParams() throws ServletException, IOException {
        when(getRequest.getParameter("lastPage")).thenReturn("incorrectPath");
        createTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        assertThat(getParam("lastPage")).isEqualTo(getDefaultPage());
    }

    @Test
    public void doGet_noLastPage() throws ServletException, IOException {
        when(getRequest.getHeader("Referer")).thenReturn(null);
        createTagServlet.doGetTestable(getRequest, getResponse);
        captureParams();
        assertThat(getParam("lastPage")).isEqualTo(getDefaultPage());
    }

    @Test
    public void doGet_notAllowed() throws ServletException, IOException {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        createTagServlet.doGetTestable(getRequest, getResponse);
        verify(getResponse).sendError(401, NOT_ALLOWED_MSG);
    }

    @Test
    public void doPost_notAllowed() throws ServletException, IOException {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        createTagServlet.doPostTestable(postRequest, postResponse);
        verify(postResponse).sendError(401, NOT_ALLOWED_MSG);
    }

    private String getDefaultPage() {
        return BASE_URL + "/plugins/servlet/view-tags?projKey=" + PROJECT_KEY + "&repo=" + repoSlug;
    }

    private void invalidResult() {
        doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CreateTagFormResult results = (CreateTagFormResult) args[0];
                results.addError("anyField", "message");
                return "adding error";
            }
        }).when(createTagFormValidator).validate(any(CreateTagFormResult.class));
    }

    @Override
    protected AbstractTagServlet getServlet() {
        return createTagServlet;
    }


    private CreateTagFormResult getCreateTagFormResult() {
        return (CreateTagFormResult) getParam("formResult");
    }
}
