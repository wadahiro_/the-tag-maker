package ut.be.contribute.atlassian.servlet;

import be.contribute.atlassian.service.RepositoryResolver;
import be.contribute.atlassian.servlet.AbstractTagServlet;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.junit.Before;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Random;

import static junit.framework.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;


public abstract class AbstractTagServletTest {

    protected static final String PROJECT_KEY = "proj_X";
    protected static final String BASE_URL = "http://baseurl/context";


    @Mock
    protected SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    protected RepositoryService repositoryService;
    @Mock
    protected RepositoryResolver repositoryResolver;
    @Mock
    protected ApplicationPropertiesService applicationPropertiesService;

    @Mock
    protected HttpServletRequest postRequest;
    @Mock
    protected HttpServletRequest getRequest;
    @Mock
    protected HttpServletResponse postResponse;
    @Mock
    protected HttpServletResponse getResponse;
    @Captor
    protected ArgumentCaptor<Map<String, Object>> mapArgumentCaptor;
    @Mock
    protected Repository repository ;
    @Mock
    protected Repository personalRepository ;
    @Mock
    protected Project project;


    protected int repoId = new Random().nextInt();
    protected String repoSlug;
    protected URI baseUrl;


    @Before
    public void before() throws URISyntaxException {
        baseUrl = new URI(BASE_URL);
        repoSlug = "rep_" + repoId;
       // when(repositoryService.getBySlug(getProjectKey, repoSlug)).thenReturn(repository);
        when(repositoryResolver.resolveRepository(PROJECT_KEY, repoSlug)).thenReturn(repository);
        when(repositoryResolver.resolveRepository(PROJECT_KEY, repoSlug, false)).thenReturn(repository);
        when(repositoryResolver.resolveRepository(PROJECT_KEY, repoSlug, true)).thenReturn(personalRepository);
        when(repositoryService.getById(repoId)).thenReturn(repository);
        when(repository.getProject()).thenReturn(project);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(repository.getSlug()).thenReturn(repoSlug);
        when(applicationPropertiesService.getBaseUrl()).thenReturn(baseUrl);


    }

    protected void assertCorrectTemplate(String templateName) {
        try {
            verify(soyTemplateRenderer).render(eq(getResponse.getWriter()),
                    eq(getServlet().getResource()),
                    eq(templateName),
                    mapArgumentCaptor.capture()
            );
        } catch (SoyException e) {
            fail();
        } catch (IOException e) {
            fail();
        }
    }

    protected abstract AbstractTagServlet getServlet() ;

    protected void captureParams() {
        try {
            verify(soyTemplateRenderer).render(any(Appendable.class),
                    anyString(),
                    anyString(),
                    mapArgumentCaptor.capture()
            );
        } catch (SoyException e) {
        }
    }

    protected void hasParam(String key, Object value) {
        assertThat(mapArgumentCaptor.getValue().get(key)).isEqualTo(value);
    }

    protected Object getParam(String key) {
       return mapArgumentCaptor.getValue().get(key);
    }
}
