package ut.be.contribute.atlassian.service;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.service.ViewableManager;
import be.contribute.atlassian.service.ViewableManagerImpl;
import be.contribute.atlassian.web.ChangesetViewable;
import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.user.Person;
import com.atlassian.bitbucket.util.DateFormatter;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static com.atlassian.bitbucket.util.DateFormatter.FormatType.LONG;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ViewableManagerImplTest {
    private static final String EMAIL = "email";
    private static final String NAME = "name";
    private static final String AVATAR_URL = "avatarUrl";
    private static final String SHORT_HASH = "abc123";
    private static final String COMMIT_HASH = "aaaabbbbccccc";
    private static final String[] DESCRIPTION = new String[]{"desc1", "desc2"};
    private static final String TAG_NAME = "tagName";
    private static final String DEFAULT_AVATAR = "defaultAvatar";
    private static final String PATH = "urlPath";
    private static final String FORMATTED_DATE = "formattedDate";
    private static final String MESSAGE = "Message";
    private static final String LINK_URL = "linkUrl";
    private final DetailedTag detailedTag = new DetailedTag();
    private final DateTime taggedDateTime = new DateTime();
    private Tagger tagger = new Tagger(NAME, EMAIL);
    private ArrayList<DetailedTag> detailedTags = new ArrayList<DetailedTag>();
    private ViewableManager viewableManager;


    @Mock
    private AvatarService avatarService;
    @Mock
    private CommitService commitService;
    @Mock
    private Repository repository;
    @Mock
    private Commit changeset;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private NavBuilder navBuilder;
    @Mock
    private Project project;
    @Mock
    private DateFormatter dateFormatter;
    @Mock
    private Person mockPerson;


    @Before
    public void setUp() {
        detailedTag.setTagger(tagger);
        detailedTags.add(detailedTag);
        detailedTag.setCommitHash(COMMIT_HASH);
        detailedTag.setDescription(DESCRIPTION);
        detailedTag.setTagName(TAG_NAME);
        detailedTag.setTaggedDateTime(taggedDateTime);
        viewableManager = new ViewableManagerImpl(avatarService, commitService, navBuilder, dateFormatter);
        when(avatarService.getUrlForPerson(eq(tagger), any(AvatarRequest.class))).thenReturn(AVATAR_URL);
        when(avatarService.getUrlForPerson(eq(mockPerson), any(AvatarRequest.class))).thenReturn(AVATAR_URL);
        when(commitService.getCommit(new CommitRequest.Builder(repository, COMMIT_HASH).build())).thenReturn(changeset);
        when(changeset.getDisplayId()).thenReturn(SHORT_HASH);
        when(changeset.getId()).thenReturn(COMMIT_HASH);
        //when(navBuilder.buildAbsolute())
        when(repository.getProject()).thenReturn(project);
        when(
                navBuilder.project(repository.getProject())
                        .repo(repository)
                        .commit(COMMIT_HASH)
                        .buildAbsolute()
        ).thenReturn(PATH);
        when(
                navBuilder.project(repository.getProject())
                        .repo(repository)
                        .browse()
                        .atRevision("refs/tags/" + TAG_NAME)
                        .buildAbsolute()
        ).thenReturn(LINK_URL);

        when(changeset.getAuthorTimestamp()).thenReturn(taggedDateTime.toDate());
        when(changeset.getAuthor()).thenReturn(mockPerson);
        when(changeset.getMessage()).thenReturn(MESSAGE);
        when(mockPerson.getName()).thenReturn(NAME);
        when(dateFormatter.formatDate(taggedDateTime.toDate(), LONG)).thenReturn(FORMATTED_DATE);
    }

    @Test
    public void changeset_hasCorrectHash() {
       assertThat(getChangeset().getCommitHash()).isEqualTo(COMMIT_HASH);
    }

    @Test
    public void changeset_hasCorrectShortHash() {
        assertThat(getChangeset().getShortCommitHash()).isEqualTo(SHORT_HASH);
    }

    @Test
    public void changeset_hasCorrectUrl() {
        assertThat(getChangeset().getCommitUrl()).isEqualTo(PATH);
    }

    @Test
    public void changeset_hasCorrectDate() {
        assertThat(getChangeset().getCommitDate()).isEqualTo(FORMATTED_DATE);
    }

    @Test
    public void changeset_hasCorrectAvatar() {
        assertThat(getChangeset().getAvatarUrl()).isEqualTo(AVATAR_URL);
    }

    @Test
    public void changeset_hasCorrectName() {
        assertThat(getChangeset().getName()).isEqualTo(NAME);
    }

    @Test
    public void changeset_hasCorrectMessage() {
        assertThat(getChangeset().getMessage()).isEqualTo(MESSAGE);
    }

    private ChangesetViewable getChangeset() {
        return viewableManager.createViewAble(changeset, repository);
    }

}
