package ut.be.contribute.atlassian.service;


import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.command.CustomCommandFactory;
import be.contribute.atlassian.event.TagCreatedEvent;
import be.contribute.atlassian.event.TagDeletedEvent;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.service.TagServiceImpl;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitRefCommandFactory;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.scm.Command;
import com.atlassian.bitbucket.scm.ScmCommandFactory;
import com.atlassian.bitbucket.scm.ScmService;
import com.atlassian.bitbucket.scm.TagsCommandParameters;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.ref.GitCreateTagCommandParameters;
import com.atlassian.bitbucket.scm.git.ref.GitDeleteTagCommandParameters;
import com.atlassian.bitbucket.scm.ref.CreateTagCommandParameters;
import com.atlassian.bitbucket.scm.ref.ScmRefCommandFactory;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

    private static final String EMPTY_HASH = Strings.repeat("0", 40);

    private static final String COMMIT_ID = "a3b576dv";
    private static final String TAG_NAME = "v1.1.1";
    private static final String DESCRIPTION = "First version since blah balh";
    private static final String TAG_ID = "tag_id";
    private static final String NEW_TAG_NAME = "newName";
    private final ArrayList<Tag> tagList = new ArrayList<Tag>();
    private final int listlength = new Random().nextInt(15) + 1;
    private final DetailedTag detailedTag = new DetailedTag();
    private TagService tagService;

    @Mock
    private Repository repository;
    @Mock
    private ScmService scmService;
    @Mock
    private CustomCommandFactory customCommandFactory;
    @Mock
    private Command<Page<Tag>> tags;
    @Mock
    private PageRequest pageRequest;
    @Mock
    private Page<Tag> tagPage;
    @Mock
    private Tag mockTag;
    @Mock
    private Command<DetailedTag> detailedTagCommand;
    @Mock
    private ScmCommandFactory scmCommandFactory;
    @Mock
    private ScmRefCommandFactory scmRefCommandFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private GitCommand<Tag> command;
    @Mock
    private Tag tag;
    @Mock
    private GitScm gitScm;
    @Mock
    private GitCommand<Void> deleteTagCommand;
    @Mock
    private GitCommand<Tag> createCommand;
    @Captor
    private ArgumentCaptor<CreateTagCommandParameters> createTagCommandParametersArgumentCaptor;
    @Captor
    private ArgumentCaptor<TagCreatedEvent> tagCreatedEventArgumentCaptor;
    @Mock
    private GitRefCommandFactory gitRefCommandFactory;
    @Captor
    private ArgumentCaptor<TagDeletedEvent> tagDeletedEventArgumentCaptor;
    @Captor
    private ArgumentCaptor<GitDeleteTagCommandParameters> deleteCaptor;
    @Captor
    private ArgumentCaptor<GitCreateTagCommandParameters> gitCreateParameters;

    @Before
    public void setUp() {

        initTagList();
        detailedTag.setCommitHash(COMMIT_ID);
        detailedTag.setTagName(TAG_NAME);
        when(scmService.getCommandFactory(repository)).thenReturn(scmCommandFactory);
        when(scmService.getRefCommandFactory(repository)).thenReturn(scmRefCommandFactory);
        when(scmCommandFactory.tags(any(TagsCommandParameters.class), eq(pageRequest))).thenReturn(tags);
        when(tags.call()).thenReturn(tagPage);
        when(tagPage.getValues()).thenReturn(tagList);
        when(customCommandFactory.tagInformation(repository, TAG_NAME)).thenReturn(detailedTagCommand);
        when(detailedTagCommand.call()).thenReturn(detailedTag);

        when(mockTag.getDisplayId()).thenReturn(TAG_NAME);
        when(gitRefCommandFactory.createTag(eq(repository) ,any(CreateTagCommandParameters.class))).thenReturn(command);

        when(createCommand.call()).thenReturn(tag);
        when(tag.getHash()).thenReturn(COMMIT_ID);
        when(tag.getId()).thenReturn(TAG_ID);

        when(gitScm.getRefCommandFactory()).thenReturn(gitRefCommandFactory);
        when(gitRefCommandFactory.deleteTag(any(Repository.class), any(GitDeleteTagCommandParameters.class))).thenReturn(deleteTagCommand);
        when(gitRefCommandFactory.createTag(any(Repository.class), any(GitCreateTagCommandParameters.class))).thenReturn(createCommand);

        tagService = new TagServiceImpl(customCommandFactory, scmService, eventPublisher, gitRefCommandFactory, gitScm);
    }

    @Test
    public void getTags_notNull() {
        assertThat(getTags()).isNotNull();
    }

    @Test
    public void getTags_getListOfTags() {
        getTags();
        verify(scmCommandFactory).tags(any(TagsCommandParameters.class), eq(pageRequest));
    }

    @Test
    public void getTags_sameDetailedTagsAsTags() {
        assertThat(getTags().getSize()).isEqualTo(tagList.size());
    }

    @Test
    public void getTags_getDetailedTagsForTags() {
        getTags();
        verify(customCommandFactory, times(listlength)).tagInformation(repository, TAG_NAME);
    }

    @Test
    public void getTags_callsCommandForEachTag() {
        getTags();
        verify(detailedTagCommand, times(listlength)).call();
    }

    @Test
    public void getTags_returnsCorrectDetailedTags() {
        assertThat(getTags().getValues()).contains(detailedTag);
    }

    @Test
    public void createTag_usesScmRefCommandFactory() throws ServletException, IOException {
        createTag();
        verify(gitRefCommandFactory).createTag(eq(repository), createTagCommandParametersArgumentCaptor.capture());
        CreateTagCommandParameters createTagCommandParameters = createTagCommandParametersArgumentCaptor.getValue();
        assertThat(createTagCommandParameters.getStartPoint()).isEqualTo(COMMIT_ID);
        assertThat(createTagCommandParameters.getName()).isEqualTo(TAG_NAME);
        assertThat(createTagCommandParameters.getMessage()).isEqualTo(DESCRIPTION);
    }

    @Test
    public void createTag_callsCommand() {
        createTag();
        verify(createCommand).call();
    }

    @Test
    public void createTag_publishesEvent() {
        createTag();
        verify(eventPublisher).publish(tagCreatedEventArgumentCaptor.capture());
        TagCreatedEvent tagCreatedEvent = tagCreatedEventArgumentCaptor.getValue();
        assertThat(tagCreatedEvent.getRepository()).isEqualTo(repository);
        assertThat(tagCreatedEvent.getSource()).isEqualTo(tagService);
        ArrayList<RefChange> refChanges = Lists.newArrayList(tagCreatedEvent.getRefChanges());
        RefChange refChange = refChanges.get(0);
        assertThat(refChange.getRefId()).isEqualTo(TAG_ID);
        assertThat(refChange.getFromHash()).isEqualTo(COMMIT_ID);
        assertThat(refChange.getToHash()).isEqualTo(EMPTY_HASH);
        assertThat(refChange.getType()).isEqualTo(RefChangeType.UPDATE);
    }

    @Test
    public void deleteTag_usesScmRefCommandFactory() {
        deleteTag();
        verify(gitRefCommandFactory).deleteTag(eq(repository), deleteCaptor.capture());
        GitDeleteTagCommandParameters params = deleteCaptor.getValue();
        assertThat(params.getName()).isEqualTo(TAG_NAME);
    }

    @Test
    public void deleteTag_callsCommand() {
        deleteTag();
        verify(deleteTagCommand).call();
    }

    @Test
    public void updateTag_usesCorrectParams() {
        update();
        verify(gitRefCommandFactory).createTag(eq(repository), gitCreateParameters.capture());
        GitCreateTagCommandParameters parameters = gitCreateParameters.getValue();
        assertThat(parameters.isForce()).isTrue();
        assertThat(parameters.getName()).isEqualTo(TAG_NAME);
        assertThat(parameters.getMessage()).isEqualTo(DESCRIPTION);
        assertThat(parameters.getStartPoint()).isEqualTo(COMMIT_ID);
    }

    @Test
    public void update_callsCreateCommand() {
       update();
        verify(createCommand).call();
    }

    @Test
    public void rename_deletesTag() {
        rename();
        verify(deleteTagCommand).call();
    }

    @Test
    public void rename_correctCreateParams() {
        rename();
        verify(gitRefCommandFactory).createTag(eq(repository),createTagCommandParametersArgumentCaptor.capture());
        CreateTagCommandParameters createTagCommandParameters = createTagCommandParametersArgumentCaptor.getValue();
        assertThat(createTagCommandParameters.getStartPoint()).isEqualTo(COMMIT_ID);
        assertThat(createTagCommandParameters.getName()).isEqualTo(NEW_TAG_NAME);
        assertThat(createTagCommandParameters.getMessage()).isEqualTo(DESCRIPTION);
    }

    @Test
    public void deleteTag_publishesDeleteTagEvent() {
        deleteTag();
        verify(eventPublisher).publish(tagDeletedEventArgumentCaptor.capture());
        TagDeletedEvent value = tagDeletedEventArgumentCaptor.getValue();
        assertThat(value.getRepository()).isEqualTo(repository);
        assertThat(value.getSource()).isEqualTo(tagService);
        ArrayList<RefChange> refChanges = Lists.newArrayList(value.getRefChanges());
        RefChange refChange = refChanges.get(0);
        assertThat(refChange.getRefId()).isEqualTo(detailedTag.getResolvedTagId());
        assertThat(refChange.getFromHash()).isEqualTo(COMMIT_ID);
        assertThat(refChange.getToHash()).isEqualTo(EMPTY_HASH);
        assertThat(refChange.getType()).isEqualTo(RefChangeType.DELETE);
    }

    @Test
    public void getTag_getsTagInformation() {
        getTag();
        verify(customCommandFactory).tagInformation(repository, TAG_NAME);
    }

    @Test
    public void getTag_callsCommand() {
        getTag();
        verify(detailedTagCommand).call();
    }

    @Test
    public void getTag_returnsDetailedTag() {
        assertThat(getTag()).isEqualTo(detailedTag);
    }

    @Test
    public void updateTag_publishesEvent() {
        update();
        verify(eventPublisher).publish(tagCreatedEventArgumentCaptor.capture());
        TagCreatedEvent tagCreatedEvent = tagCreatedEventArgumentCaptor.getValue();
        assertThat(tagCreatedEvent.getRepository()).isEqualTo(repository);
        assertThat(tagCreatedEvent.getSource()).isEqualTo(tagService);
        ArrayList<RefChange> refChanges = Lists.newArrayList(tagCreatedEvent.getRefChanges());
        RefChange refChange = refChanges.get(0);
        assertThat(refChange.getRefId()).isEqualTo(TAG_ID);
        assertThat(refChange.getFromHash()).isEqualTo(COMMIT_ID);
        assertThat(refChange.getToHash()).isEqualTo(EMPTY_HASH);
        assertThat(refChange.getType()).isEqualTo(RefChangeType.UPDATE);
    }

    private DetailedTag getTag() {
        return tagService.getTag(TAG_NAME, repository);
    }

    private void rename() {
        tagService.renameTag(TAG_NAME, NEW_TAG_NAME, DESCRIPTION, COMMIT_ID, repository);
    }

    private Tag update() {
        return tagService.updateTag(TAG_NAME,DESCRIPTION, COMMIT_ID, repository);
    }

    private void deleteTag() {
        tagService.deleteTag(TAG_NAME, repository);
    }

    private void createTag() {
        tagService.createTag(TAG_NAME, DESCRIPTION, COMMIT_ID, repository);
    }

    private Page<DetailedTag> getTags() {
        return tagService.getTags(repository, pageRequest);
    }

    private void initTagList() {
        for (int i = 0; i < listlength; i++) {
            tagList.add(mockTag);
        }
    }

}
