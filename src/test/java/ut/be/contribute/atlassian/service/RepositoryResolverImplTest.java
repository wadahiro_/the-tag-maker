package ut.be.contribute.atlassian.service;

import be.contribute.atlassian.service.RepositoryResolverImpl;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryResolverImplTest {

    private static final String REPO_SLUG = "personal";
    private static final String PROJECT_KEY = "admin";
    private static final String USER_KEY = "~admin";
    private RepositoryResolverImpl repositoryResolver;
    @Mock
    private Repository normalRepository;
    @Mock
    private Repository personalRepository;
    @Mock
    private RepositoryService repositoryService;

    @Before
    public void setUp() {
        repositoryResolver = new RepositoryResolverImpl(repositoryService);
        when(repositoryService.getBySlug(PROJECT_KEY, REPO_SLUG)).thenReturn(normalRepository);
        when(repositoryService.getBySlug(USER_KEY, REPO_SLUG)).thenReturn(personalRepository);
    }

    @Test
    public void personal_returnsPersonal() {
        Repository repository = repositoryResolver.resolveRepository("admin", "personal", true);
        assertThat(repository).isEqualTo(personalRepository);
    }

    @Test
    public void normal_returnsPersonal() {
        Repository repository = repositoryResolver.resolveRepository("admin", "personal", false);
        assertThat(repository).isEqualTo(normalRepository);
    }

    @Test
    public void normal_returnsNormalNoExtraParams() {
        Repository repository = repositoryResolver.resolveRepository("admin", "personal");
        assertThat(repository).isEqualTo(normalRepository);
    }
}
