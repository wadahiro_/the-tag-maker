package ut.be.contribute.atlassian.validator;

import be.contribute.atlassian.command.CustomCommandFactory;
import be.contribute.atlassian.validator.CreateTagFormValidatorImpl;
import be.contribute.atlassian.web.CreateTagFormResult;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scm.Command;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CreateTagFormValidatorImplTest {

    private static final int REPO_ID = 1;
    private static final String COMMIT_ID = "c3d2b54x2";
    private static final String TAG_NAME = "tagName";
    private static final String DESCRIPTION = "description";
    private static final String DUPLICATE_TAG = "duplicateTag";

    private CreateTagFormValidatorImpl createTagFormValidator;

    @Mock
    private CustomCommandFactory customCommandFactory;
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private Repository repository;
    @Mock
    private Command<Boolean> trueCommand;
    @Mock
    private Command<Boolean> falseCommand;

    private CreateTagFormResult createTagFormResult = new CreateTagFormResult(REPO_ID, COMMIT_ID, TAG_NAME, DESCRIPTION);

    @Before
    public void setUp() throws Exception {
        createTagFormValidator = new CreateTagFormValidatorImpl(repositoryService, customCommandFactory);

        when(repositoryService.getById(REPO_ID)).thenReturn(repository);
        when(customCommandFactory.tagByName(eq(repository), anyString())).thenReturn(falseCommand);
        when(customCommandFactory.tagByName(repository, null)).thenThrow(new NullPointerException());
        when(customCommandFactory.tagByName(repository, DUPLICATE_TAG)).thenReturn(trueCommand);
        when(trueCommand.call()).thenReturn(true);
        when(falseCommand.call()).thenReturn(false);

    }

    @Test
    public void validate_addsErrorOnEmptyDescription() {
        createTagFormResult.setDescription(null);
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("description")).isEqualTo("please provide a description for the tag");
    }

    @Test
    public void validate_addsErrorOnEmptyTagName() {
        createTagFormResult.setTagName(null);
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("please provide a name for the tag");
    }

    @Test
    public void validate_addsErrorWhenTagNameAlreadyUsed() {
        createTagFormResult.setTagName(DUPLICATE_TAG);
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("this name is already used for a tag");
    }

    @Test
    public void validate_notChecksTagNameWhenEmpty() {
        createTagFormResult.setTagName(null);
        createTagFormValidator.validate(createTagFormResult);
       verify(customCommandFactory, never()).tagByName(any(Repository.class), anyString());
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsSpaces() {
        createTagFormResult.setTagName("bananen en frigoxen");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain spaces");
    }

    @Test
    public void validate_addsErrorWhenTagNameStartWithDot() {
        createTagFormResult.setTagName(".bananen");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot start with a .");
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsDoubleDot() {
        createTagFormResult.setTagName("frigo..box");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain a double dot (..)");
    }

    @Test
    public void validate_addsErrorWhenTagNameEndsWithForwardslash() {
        createTagFormResult.setTagName("bananen/");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot end with a /");
    }

    @Test
    public void validate_addsErrorWhenTagNameEndsWithDotLock() {
        createTagFormResult.setTagName("bananen.lock");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot end with .lock");
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsBackslash() {
        createTagFormResult.setTagName("frigo\\box");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain a \\");
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsTilde() {
        createTagFormResult.setTagName("frigob~ox");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain a ~");
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsPartyHat() {
        createTagFormResult.setTagName("frigobox^ss");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain a ^");
    }

    @Test
    public void validate_addsErrorWhenTagNameContainsDoublePoint() {
        createTagFormResult.setTagName("frigo:box");
        createTagFormValidator.validate(createTagFormResult);
        assertThat(createTagFormResult.hasErrors()).isTrue();
        assertThat(createTagFormResult.getErrors().get("tagName")).isEqualTo("name of the tag cannot contain a :");
    }
}
