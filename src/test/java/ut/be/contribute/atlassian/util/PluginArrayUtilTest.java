package ut.be.contribute.atlassian.util;

import be.contribute.atlassian.util.PluginArrayUtil;
import org.junit.Test;

import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;


public class PluginArrayUtilTest {

    @Test
    public void append_appendsCorrect() {
        String[] append = PluginArrayUtil.append(new String[]{"1", "2"}, "3");
        assertThat(append).containsExactly("1", "2","3");
    }
}
