package ut.be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.enums.RefType;
import be.contribute.atlassian.enums.TagType;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.DetailedTagRestTransformFunction;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.rest.commit.RestCommit;
import com.atlassian.bitbucket.rest.repository.RestTag;
import com.atlassian.bitbucket.user.Person;
import com.atlassian.bitbucket.util.DateFormatter;
import com.atlassian.bitbucket.util.Page;
import org.fest.util.Arrays;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DetailedTagRestTransformFunctionTest {

    DetailedTagRestTransformFunction function;

    private static final String TAG_NAME = "tag_name";
    private static final String LINK_URL = "linkUrl";
    private static final String PATH = "path";
    private static final String COMMIT_ID = "a1b2c3";
    private static final String TAGGER_EMAIL = "email";
    private static final String TAGGER_NAME = "taggerName";
    private static final DateTime NOW_DATE_TIME = new DateTime();
    private static final String TAGGED_TAG_NAME = "pointed tag";
    private static final String[] DESCRIPTION = Arrays.array("descline1", "descline2");
    private static final String TAGGED_LINK_URL = "taggedLinkUrl";
    private static final String FORMATTED_DATE = "formattedDate";
    private DetailedTag detailedTag;

    @Mock
    private Repository repository;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private NavBuilder navBuilder;
    @Mock
    private Project project;
    @Mock
    private CommitService commitService;
    @Mock
    private Commit commit;
    @Mock
    private AvatarService avatarService;
    @Mock
    private Tag tag;
    @Mock
    private RefService refService;
    @Mock
    private DateFormatter dateFormatter;

    private Tagger tagger;

    private Page<DetailedTag> detailedTagPage;


    @Before
    public void setUp() {
        function = new DetailedTagRestTransformFunction(navBuilder, commitService, avatarService, refService, dateFormatter);
        function.setRepository(repository);

        tagger = new Tagger(TAGGER_NAME, TAGGER_EMAIL);
        detailedTag = new DetailedTag();
        detailedTag.setTagName(TAG_NAME);
        detailedTag.setCommitHash(COMMIT_ID);
        detailedTag.setTagType(TagType.ANNOTATED);
        detailedTag.setTagger(tagger);
        detailedTag.setTaggedDateTime(NOW_DATE_TIME);
        detailedTag.setDescription(DESCRIPTION);

        when(repository.getProject()).thenReturn(project);
        when(
                navBuilder.project(repository.getProject())
                        .repo(repository)
                        .commit(COMMIT_ID)
                        .buildAbsolute()
        ).thenReturn(PATH);
        when(
                navBuilder.project(repository.getProject())
                        .repo(repository)
                        .browse()
                        .atRevision("refs/tags/" + TAG_NAME)
                        .buildAbsolute()
        ).thenReturn(LINK_URL);
        when(
                navBuilder.project(repository.getProject())
                        .repo(repository)
                        .browse()
                        .atRevision("refs/tags/" + TAGGED_TAG_NAME)
                        .buildAbsolute()
        ).thenReturn(TAGGED_LINK_URL);


        when(refService.resolveRef(repository, TAGGED_TAG_NAME)).thenReturn(tag);

        when(commitService.getCommit(new CommitRequest.Builder(repository, COMMIT_ID).build())).thenReturn(commit);
        when(dateFormatter.formatDate(NOW_DATE_TIME.toDate(), DateFormatter.FormatType.LONG)).thenReturn(FORMATTED_DATE);

        when(tag.getDisplayId()).thenReturn(TAGGED_TAG_NAME);
        when(commit.getDisplayId()).thenReturn(COMMIT_ID);
    }

    @Test(expected = NullPointerException.class)
    public void function_RepositoryNotNull() {
        function.setRepository(null);
        transform();
    }

    @Test
    public void create_setsSelfUrl() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getSelf()).isEqualTo(LINK_URL);
    }

    @Test
    public void create_setsValues() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getType()).isEqualTo(detailedTag.getTagType().name());
    }

    @Test
    public void create_setsChangeset() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getCommit()).isEqualTo(getExpectedRestChangeset());
    }

    @Test
    public void create_setsTagger() {
        DetailedTagRestResource resource = transform();
        Person tagger = resource.getTagger();
        assertThat(tagger.getEmailAddress()).isEqualTo(TAGGER_EMAIL);
        assertThat(tagger.getName()).isEqualTo(TAGGER_NAME);
    }

    @Test
    public void create_setsTagName() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getName()).isEqualTo(TAG_NAME);
    }

    @Test
    public void create_setsTaggedOn() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getTaggedOn()).isEqualTo(FORMATTED_DATE);
    }

    @Test
    public void create_setsPointsToType() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getPointsToType()).isEqualTo(RefType.COMMIT);
        assertThat(resource.getPointsTo()).isEqualTo(COMMIT_ID);
    }

    @Test
    public void create_setsPointingTagInfo() {
        detailedTag.setTaggedTagName(TAGGED_TAG_NAME);

        DetailedTagRestResource resource = transform();
        assertThat(resource.getPointedTag()).isEqualTo(getExpectedRestTag());
        assertThat(resource.getPointsTo()).isEqualTo(TAGGED_TAG_NAME);
        assertThat(resource.getPointsToType()).isEqualTo(RefType.TAG);
    }

    @Test
    public void create_setsDescription() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getDescription()).isEqualTo(DESCRIPTION);
    }

    @Test
    public void create_setsTaggedLinkUrl() {
        detailedTag.setTaggedTagName(TAGGED_TAG_NAME);

        DetailedTagRestResource resource = transform();
        assertThat(resource.getPointedTag().get("self")).isEqualTo(TAGGED_LINK_URL);
    }

    @Test
    public void create_setsChangesetUrl() {
        DetailedTagRestResource resource = transform();
        assertThat(resource.getCommit().get("self")).isEqualTo(PATH);
    }

    @Test
    public void pointsTo_DeletedTag() {
        detailedTag.setTaggedTagName(TAGGED_TAG_NAME);
        when(refService.resolveRef(repository, TAGGED_TAG_NAME)).thenReturn(null);
        DetailedTagRestResource resource = transform();
        assertThat(resource.getPointedTag().get("self")).isNull();
        //assertThat(resource.getPointedTag().get("id")).isEqualTo("");
        assertThat(resource.getPointedTag().get("displayId")).isEqualTo(TAGGED_TAG_NAME);
    }

    @Test
    public void lightweight_noDate() {
        detailedTag.setTaggedDateTime(null);
        DetailedTagRestResource resource = transform();

        assertThat(resource.getTaggedOn()).isEqualTo("");
    }

    @Test
    public void lightweight_noTagger() {
        detailedTag.setTagger(null);
        DetailedTagRestResource resource = transform();

        assertThat(resource.getTagger()).isNullOrEmpty();
    }

    @Test
    public void unkownPointer_noTagOrCommit() {
        detailedTag.setCommitHash(null);
        detailedTag.setTaggedTagName(null);
        DetailedTagRestResource detailedTagRestResource = transform();
        assertThat(detailedTagRestResource.getPointsToType()).isEqualTo(RefType.NONE);
    }

    private DetailedTagRestResource transform() {
        return function.apply(detailedTag);
    }

    private RestTag getExpectedRestTag() {
        RestTag restTag = new RestTag(tag);
        restTag.put("self", TAGGED_LINK_URL);
        return restTag;
    }

    private RestCommit getExpectedRestChangeset() {
        RestCommit restChangeset = new RestCommit(commit);
        restChangeset.put("self", PATH);
        return restChangeset;
    }

}
