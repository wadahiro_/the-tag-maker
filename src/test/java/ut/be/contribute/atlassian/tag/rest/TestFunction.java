package ut.be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.DetailedTagRestTransformFunction;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.util.DateFormatter;

import javax.annotation.Nullable;


public class TestFunction extends DetailedTagRestTransformFunction {

    private DetailedTagRestResource result;

    public TestFunction(DetailedTagRestResource result) {
        super(null, null, null, null, null);
        this.result = result;
    }

    public TestFunction(NavBuilder navBuilder, CommitService commitService, AvatarService avatarService, RefService refService, DateFormatter dateFormatter) {
        super(navBuilder, commitService, avatarService, refService, dateFormatter);
    }

    @Nullable
    @Override
    public DetailedTagRestResource apply(@Nullable DetailedTag input) {
        return result;
    }
}
