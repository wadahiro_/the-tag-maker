package ut.be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.enums.TagType;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.TagRestEndpoint;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactory;
import be.contribute.atlassian.tag.rest.UpdateTagRestResource;
import be.contribute.atlassian.validator.RestValidationResult;
import be.contribute.atlassian.validator.TagRestValidator;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.InvalidAuthorException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.rest.util.RestPage;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageImpl;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagRestEndpointTest {

    private static final String REPO_SLUG = "rep_1";
    private static final String PROJECT_KEY = "proj_1";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "desc";
    private static final String COMMIT_ID = "a1b2c3";
    private static final String EMAIL = "email";
    private static final String USER = "user";
    private static final DateTime TIME = new DateTime();

    private TagRestEndpoint tagRestEndpoint;

    private UpdateTagRestResource updateResource = new UpdateTagRestResource();

    @Mock
    private Repository repository;

    @Mock
    private RepositoryService repositoryService;

    @Mock
    private TagService tagService;
    @Mock
    private TagRestValidator tagRestValidator;
    @Mock
    private Tag internalTag;

    @Mock
    private PermissionService permissionService;


    @Mock
    private TagRestResourceTransformationFactory tagRestResourceTransformationFactory;

    private DetailedTag tag = new DetailedTag();
    private RestValidationResult result = new RestValidationResult();
    private DetailedTagRestResource detailedTagRestResource = new DetailedTagRestResource();
    private PageRequest pageRequest;
    private Page<DetailedTag> tagPage;
    private Page<DetailedTagRestResource> resultPage;


    @Before
    public void setUp() {
        tagRestEndpoint = new TagRestEndpoint(tagService, repositoryService, tagRestValidator, tagRestResourceTransformationFactory, permissionService);
        when(repositoryService.getBySlug(PROJECT_KEY, REPO_SLUG)).thenReturn(repository);
        when(tagService.getTag(NAME, repository)).thenReturn(tag);
        when(tagService.updateTag(NAME, DESCRIPTION, COMMIT_ID, repository)).thenReturn(internalTag);

        updateResource.setCommitId(COMMIT_ID);
        updateResource.setName(NAME);
        updateResource.setProjectKey(PROJECT_KEY);
        updateResource.setRepoSlug(REPO_SLUG);
        updateResource.setMessage(DESCRIPTION);

        tag.setCommitHash(COMMIT_ID);
        tag.setTagName(NAME);
        tag.setTagger(new Tagger(USER, EMAIL));
        tag.setTaggedDateTime(TIME);
        tag.setTagType(TagType.ANNOTATED);

        when(tagRestValidator.validate(updateResource)).thenReturn(result);
        when(internalTag.getDisplayId()).thenReturn(NAME);

        when(tagRestResourceTransformationFactory.getTransformation(repository)).thenReturn(new TestFunction(detailedTagRestResource));

        pageRequest = new PageRequestImpl(0, 20);

        tagPage = new PageImpl<DetailedTag>(pageRequest, Lists.newArrayList(tag), true);
        when(tagService.getTags(repository, pageRequest)).thenReturn(tagPage);
        resultPage = new PageImpl<DetailedTagRestResource>(pageRequest, Lists.newArrayList(detailedTagRestResource), true);
        //when(tagRestResourceTransformationFactory.create(tag, repository)).thenReturn(detailedTagRestResource);
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(true);
    }

    @Test
    public void delete_deletesTag() {
        delete();
        verify(tagService).deleteTag(NAME, repository);
    }

    @Test
    public void delete_correctResponse() {
        Response delete = delete();
        assertThat(delete.getStatus()).isEqualTo(200);
    }

    @Test
    public void delete_401OnInvalidAuthor() {
        doThrow(new InvalidAuthorException(new KeyedMessage("", "", ""))).
                when(tagService).deleteTag(NAME, repository);
        assertThat(delete().getStatus()).isEqualTo(401);
    }

    @Test
    public void delete_500OnError() {
        doThrow(new RuntimeException()).
                when(tagService).deleteTag(NAME, repository);
        assertThat(delete().getStatus()).isEqualTo(500);
    }

    @Test
    public void update_callsUpdate() {
        update();
        verify(tagService).updateTag(NAME, DESCRIPTION, COMMIT_ID, repository);
    }

    @Test
    public void update_correctResponse() {
        assertThat(update().getStatus()).isEqualTo(200);
    }

    @Test
    public void update_validationError() {
        result.addError("test error", "");
        assertThat(update().getStatus()).isEqualTo(400);
        String errorJson = new Gson().toJson(result.getErrors());
        assertThat(update().getEntity()).isEqualTo(errorJson);
    }

    @Test
    public void update_returnsTag() {
        assertThat(update().getEntity()).isEqualTo(new Gson().toJson(detailedTagRestResource));
    }

    @Test
    public void getTag_getsTagFromService() {
        getTag();
        verify(tagService).getTag(NAME, repository);
    }

    @Test
    public void getTag_getTag() {
        Response res = getTag();
        DetailedTagRestResource tagResponse = (DetailedTagRestResource) res.getEntity();
        assertThat(detailedTagRestResource).isEqualTo(tagResponse);
    }

    @Test
    public void getTags_getsPagedTags() {
        Response resp = tagRestEndpoint.getTags(PROJECT_KEY, REPO_SLUG, pageRequest);
        RestPage<DetailedTagRestResource> restResult = (RestPage<DetailedTagRestResource>) resp.getEntity();
        assertThat(restResult).isEqualTo(new RestPage<DetailedTagRestResource>(resultPage));
    }

    @Test
    public void update_returns401NoEditPermission() {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        assertThat(update().getStatus()).isEqualTo(401);
    }

    @Test
    public void delete_returns401NoEditPermission() {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        assertThat(delete().getStatus()).isEqualTo(401);
    }

    private Response getTag() {
        return tagRestEndpoint.getTag(PROJECT_KEY, REPO_SLUG, NAME);
    }

    private Response update() {
        return tagRestEndpoint.update(updateResource);
    }

    private Response delete() {
        return tagRestEndpoint.delete(PROJECT_KEY, REPO_SLUG, NAME);
    }


}
