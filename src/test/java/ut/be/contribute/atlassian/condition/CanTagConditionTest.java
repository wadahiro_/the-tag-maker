package ut.be.contribute.atlassian.condition;

import be.contribute.atlassian.condition.CanTagCondition;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequestWrapper;
import java.util.HashMap;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CanTagConditionTest {
    private static final String REPO_SLUG = "rep_1";
    private static final String PROJECT_KEY = "PROJECT_1";
    private static final String CORRECT_PATH = "/projects/" + PROJECT_KEY + "/repos/" + REPO_SLUG + "/commits/6053a1eaa1c009dd11092d09a72f3c41af1b59ad";
    private static final String INCORRECT_PATH = "/projects/" + PROJECT_KEY + "/repos";
    private static final String PERSONAL_REPO = "personalKey";
    @Mock
    private RepositoryService repositoryService;
    @Mock
    private PermissionService permissionService;
    @Mock
    private HttpServletRequestWrapper requestWrapper;


    @Mock
    private Repository repository;


    private TestableCondition condition;
    private HashMap<String, Object> context;


    @Before
    public void setUp() {
        context = new HashMap<String, Object>();
        context.put("request", requestWrapper);
        condition = new TestableCondition(permissionService, repositoryService/*, licenseManager*/);
        when(requestWrapper.getServletPath()).thenReturn(CORRECT_PATH);
        when(repositoryService.getBySlug(PROJECT_KEY, REPO_SLUG)).thenReturn(repository);
        when(repositoryService.getBySlug("~" + PERSONAL_REPO, REPO_SLUG)).thenReturn(repository);
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(true);


    }

    @Test
    public void hasPermission_notOnInvalidPath() {
        when(requestWrapper.getServletPath()).thenReturn(INCORRECT_PATH);
        assertThat(condition.shouldDisplay(context)).isFalse();
    }

    @Test
    public void hasPermission_falseOnNoPermission() {
        when(permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)).thenReturn(false);
        assertThat(condition.shouldDisplay(context)).isFalse();
    }

    @Test
    public void hasPermission_trueOnCorrect() {
        assertThat(condition.shouldDisplay(context)).isTrue();
    }

    @Test
    public void hasPermission_noRepoNoError() {
        when(repositoryService.getBySlug(anyString(), anyString())).thenReturn(null);
        assertThat(condition.shouldDisplay(context)).isFalse();
        verifyNoMoreInteractions(permissionService);
    }

    @Test
    public void hasPermission_personalRepo() {
        when(requestWrapper.getServletPath()).thenReturn("/users/" + PERSONAL_REPO + "/repos/" + REPO_SLUG + "/commits/6053a1eaa1c009dd11092d09a72f3c41af1b59ad");
        assertThat(condition.shouldDisplay(context)).isTrue();
    }

    private class TestableCondition extends CanTagCondition {

        @Override
        public Permission getPermission() {
            return Permission.REPO_WRITE;
        }

        public TestableCondition(PermissionService permissionService, RepositoryService repositoryService) {
            super(permissionService, repositoryService);
        }


    }
}
