package ut.be.contribute.atlassian.command;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.PluginConstantsManager;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.command.DetailedTagCommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;

import static be.contribute.atlassian.enums.TagType.*;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DetailedTagCommandOutputHandlerTest {
    private static final String TAGGER_NAME = "Tomas Kenis";
    private static final String TAGNAME = "v1.4.0";
    private static final String TAG_LINE = "tag " + TAGNAME;
    private static final String TAGGER_EMAIL = "tomas.kenis@contribute.be";
    private static final String TAGGER = "Tagger: " + TAGGER_NAME + " <" + TAGGER_EMAIL + ">";
    private static final String TAG_DATETIME = "Wed Jul 23 14:07:50 2014 +0200";
    private static final String TAG_DATE = "Date:   " + TAG_DATETIME;
    private static final String COMMIT_AUTHOR = "Author: " + TAGGER_NAME + " <" + TAGGER_EMAIL + "> ";
    private static final String COMMIT_HASH = "db4be644ca198d6e50bce828821dfaf825384461";
    private static final String COMMIT_HASH_LINE = "commit " + COMMIT_HASH + "   ";
    private static final String TAG_DESC_1 = "First Line  ";
    private static final String TAG_DESC_2 = "Second Line  ";
    private static final String DATETIME = "Wed Jul 23 14:02:11 2014 +0200";
    private static final String COMMIT_DATE = "Date:   " + DATETIME;
    private static final String COMMIT_DESC = "some JS";
    private static final String OTHER_TAG_NAME = "otherTagName";
    private static final String SIGN_3 = "=WryJ";
    private static final String SIGN_2 = "Ki0An2JeAVUCAiJ7Ox6ZEtK+NvZAj82/";
    private static final String SIGN_1 = "iEYEABECAAYFAkmQurIACgkQON3DxfchxFr5cACeIMN+ZxLKggJQf0QYiQBwgySN";
    private static final String SIGN_VERSION = "GnuPG v1.4.8 (Darwin)";
    private DetailedTagCommandOutputHandler detailedTagCommandOutputHandler;

    @Mock
    private BufferedReader mockBufferedReader;
    @Mock
    private InputStream inputStream;

    @Before
    public void setUp() throws IOException {
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) {
                return mockBufferedReader;
            }
        };

        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
                TAGGER,
                TAG_DATE,
                "",
                TAG_DESC_1,
                TAG_DESC_2,
                "",
                COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                "",
                null);
    }

    @Test
    public void process_hasCorrectTagName() {
        process();
        assertThat(getOutput()).hasName(TAGNAME);
    }

    @Test
    public void process_hasCorrectTagger() {
        process();
        assertThat(getOutput()).hasTagger(new Tagger(TAGGER_NAME,TAGGER_EMAIL));
    }

    @Test
    public void process_hasTaggedDateTime() {
        process();
        DateTimeFormatter dtf = DateTimeFormat.forPattern(PluginConstantsManager.getGitDateFormat());
        DateTime dateTime = dtf.parseDateTime(TAG_DATETIME);

        assertThat(getOutput()).hasTaggedDateTime(dateTime);
    }

    @Test
    public void process_hasCorrectDescription() {
        process();
        assertThat(getOutput()).hasDescription(new String[]{TAG_DESC_1 ,TAG_DESC_2});
    }

    @Test
    public void process_isCorrectType() {
        process();
        assertThat(getOutput()).isType(ANNOTATED);
    }

    @Test
    public void process_hasCorrectCommitHash() {
        process();
        assertThat(getOutput()).hasCommitHash(COMMIT_HASH);
    }

    @Test
    public void process_createsCompleteDetailedTag() {
        process();
        assertThat(getOutput()).isComplete();
    }

    @Test
    public void output_isNotNull() {
        process();
        assertThat(getOutput()).isNotNull();
    }

    @Test(expected = ProcessException.class)
    public void process_IOExceptionAsProcesException() throws IOException, ProcessException {
        doThrow(new IOException()).when(mockBufferedReader).readLine();
        detailedTagCommandOutputHandler.process(inputStream);
    }

    @Test
    public void process_tag_pointsToTag() throws IOException {
        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
                TAGGER,
                TAG_DATE,
                "",
                TAG_DESC_1,
                TAG_DESC_2,
                "",
                "tag " + OTHER_TAG_NAME,
                TAGGER,
                TAG_DATE,
                COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                "",
                null);
        process();
        assertThat(getOutput()).pointsToOtherTag(OTHER_TAG_NAME);
    }
    
    @Test
    public void process_setSignatureCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasSignature(SIGN_1 + SIGN_2 + SIGN_3);
    }

    @Test
    public void process_setSignedTagTypeCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).isType(SIGNED);
    }

    @Test
    public void process_signedTagHasCorrectDescription() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasDescription(new String[]{TAG_DESC_1 ,TAG_DESC_2});
    }

    @Test
    public void process_setSignatureVersionCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasSignatureVersion(SIGN_VERSION);
    }

    private void signedTag() throws IOException {
        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
                TAGGER,
                TAG_DATE,
                "",
                TAG_DESC_1,
                TAG_DESC_2,
                "-----BEGIN PGP SIGNATURE-----",
                "Version: " + SIGN_VERSION,
                "",
                SIGN_1,
                SIGN_2,
                SIGN_3,
                "-----END PGP SIGNATURE-----",
                COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                "",
                null);
    }

    @Test
    public void lightweigtTag_isCorrect() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasCommitHash(COMMIT_HASH)
                .isType(LIGHTWEIGHT);
    }

    @Test
    public void lightweight_hasNoDate() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasTaggedDateTime(null);
    }

    @Test
    public void lightWeight_hasName() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasName(TAGNAME);
    }

    @Test(expected = ProcessException.class)
    public void noTag_throwsException() throws IOException, ProcessException {
        when(mockBufferedReader.readLine()).thenReturn(null);
        detailedTagCommandOutputHandler.process(inputStream);
    }

    private void lightweight() throws IOException {
        when(mockBufferedReader.readLine()).thenReturn(COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                null);
    }

    private DetailedTag getOutput() {
        return detailedTagCommandOutputHandler.getOutput();
    }

    private void process()  {
        try {
            detailedTagCommandOutputHandler.process(inputStream);
        } catch (ProcessException e) {
            fail();
        }
    }

}
