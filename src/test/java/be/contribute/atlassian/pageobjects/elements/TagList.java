package be.contribute.atlassian.pageobjects.elements;


import be.contribute.atlassian.pageobjects.page.tag.TagListPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.bitbucket.element.AbstractElementPageObject;
import com.atlassian.webdriver.bitbucket.element.DeleteConfirmationDialog;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.List;

public class TagList extends AbstractElementPageObject {

    @Inject
    private PageElementFinder elementFinder;

    private final TagListPage parent;

    public TagList(@Nonnull PageElement tableElement, TagListPage parent) {
        super(tableElement);
        this.parent = parent;
    }

    public TagListPage getParent() {
        return parent;
    }

    public List<TagListEntry> getRows() {
        return Lists.transform(container.findAll(By.cssSelector("tbody > tr")), new Function<PageElement, TagListEntry>() {
            @Override
            public TagListEntry apply(PageElement element) {
                return pageBinder.bind(TagListEntry.class, TagList.this, element);
            }
        });
    }

    public TagListEntry getEntryForTag(String tagName) {
        PageElement element = container.find(By.cssSelector("tbody > tr[data-tag-name=\"" + tagName + "\"]"));
        return pageBinder.bind(TagListEntry.class, TagList.this, element);
    }

    public static class TagListEntry {
        private final TagList parent;
        private final PageElement element;

        public TagListEntry(TagList parent, PageElement element) {
            this.element = element;
            this.parent = parent;
        }

        public String getTagName() {
            return getTagLink().getText();
        }

        private PageElement getTagLink() {
            return getColumn("basic-tname").find(By.tagName("a"));
        }

        public TagList getParent() {
            return parent;
        }

        public EditTagDialog clickEditTag() {
            clickAction("update-tag");
            return parent.pageBinder.bind(EditTagDialog.class, By.id("update-tag-dialog"), TimeoutType.DIALOG_LOAD,
                     new Object[]{parent.parent.getProjectKey(), parent.parent.getRepoSlug()});
        }

        public DeleteConfirmationDialog clickDeleteTag() {
            clickAction("delete-tag");
            return parent.pageBinder.bind(DeleteConfirmationDialog.class, By.id("delete-branch-dialog"), TimeoutType.DIALOG_LOAD,
                    TagListPage.class, new Object[]{parent.parent.getProjectKey(), parent.parent.getRepoSlug()});
        }

        private PageElement getColumn(String name) {
            PageElement pageElement = element.find(By.cssSelector("td[headers=" + name + "]"));
            Poller.waitUntilTrue("Column " + name + " should be present", pageElement.timed().isPresent());
            return pageElement;
        }

        private void clickAction(String className) {
            this.element.find(By.className("tag-list-actions-trigger")).click();
            PageElement menu = parent.elementFinder.find(By.id("tag-actions-menu-" + getTagName()));
            Poller.waitUntilTrue(menu.timed().isVisible());
            menu.find(By.className(className)).click();
        }

        public String getDescription() {
            return getColumn("basic-desc").getText();
        }

        public String getCommitText() {
           return getColumn("basic-commit").find(By.tagName("a")).getText();
        }
    }
}
