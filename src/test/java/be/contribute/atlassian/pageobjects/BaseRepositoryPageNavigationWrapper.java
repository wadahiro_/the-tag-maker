package be.contribute.atlassian.pageobjects;


import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.bitbucket.element.NavItem;

public abstract class BaseRepositoryPageNavigationWrapper {
    protected PageBinder pageBinder;

    protected BaseRepositoryPageNavigationWrapper(PageBinder pageBinder) {
        this.pageBinder = pageBinder;
    }

    protected <T> T navigateToWithPluginItem(NavItem item, Class<T> pageClass, Object... args) {
        item.click();
        return pageBinder.bind(pageClass, args);
    }


}
