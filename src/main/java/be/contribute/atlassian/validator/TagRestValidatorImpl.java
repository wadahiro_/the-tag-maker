package be.contribute.atlassian.validator;

import be.contribute.atlassian.tag.rest.UpdateTagRestResource;
import com.atlassian.bitbucket.NoSuchEntityException;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scm.CommandFailedException;
import org.apache.commons.lang.StringUtils;


public class TagRestValidatorImpl implements TagRestValidator {

    private final CommitService commitService;
    private final RepositoryService repositoryService;

    public TagRestValidatorImpl(CommitService commitService, RepositoryService repositoryService) {
        this.commitService = commitService;
        this.repositoryService = repositoryService;
    }

    @Override
    public RestValidationResult validate(UpdateTagRestResource resource) {
        RestValidationResult result = new RestValidationResult();

        if (StringUtils.isBlank(resource.getMessage())){
            result.addError("no message specified", "message");
        }

        if (StringUtils.isBlank(resource.getCommitId())){
            result.addError("no commit specified", "commitId");
        }  else {
            try {
                Repository repository = repositoryService.getBySlug(resource.getProjectKey(), resource.getRepoSlug());
                CommitRequest request = new CommitRequest.Builder(repository, resource.getCommitId()).build();
                commitService.getCommit(request);
            }   catch (NoSuchEntityException nsee) {
                 result.addError("invalid commit", "commitId");
            } catch (CommandFailedException e) {
                result.addError("invalid commit", "commitId");
            }
        }


        return result;
    }


}
