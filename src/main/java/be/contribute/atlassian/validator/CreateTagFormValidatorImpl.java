package be.contribute.atlassian.validator;


import be.contribute.atlassian.command.CustomCommandFactory;
import be.contribute.atlassian.web.CreateTagFormResult;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scm.Command;
import org.apache.commons.lang3.StringUtils;

public class CreateTagFormValidatorImpl implements CreateTagFormValidator {
    private RepositoryService repositoryService;
    private CustomCommandFactory customCommandFactory;

    public CreateTagFormValidatorImpl(RepositoryService repositoryService, CustomCommandFactory customCommandFactory) {
        this.repositoryService = repositoryService;
        this.customCommandFactory = customCommandFactory;
    }

    public void validate(CreateTagFormResult result) {

        if (StringUtils.isBlank(result.getDescription())) {
            result.addError("description", "please provide a description for the tag");
        }

        if (StringUtils.isBlank(result.getTagName())) {
            result.addError("tagName", "please provide a name for the tag");
        }
        else if(result.getTagName().contains(" ")){
            result.addError("tagName", "name of the tag cannot contain spaces");
        }
        else if(result.getTagName().startsWith(".")){
            result.addError("tagName", "name of the tag cannot start with a .");
        }
        else if(result.getTagName().contains("..")){
            result.addError("tagName", "name of the tag cannot contain a double dot (..)");
        }
        else if(result.getTagName().endsWith("/")){
            result.addError("tagName", "name of the tag cannot end with a /");
        }
        else if(result.getTagName().contains("\\")){
            result.addError("tagName", "name of the tag cannot contain a \\");
        }  else if(result.getTagName().endsWith(".lock")){
            result.addError("tagName", "name of the tag cannot end with .lock");
        }
        else if(result.getTagName().contains(":")){
            result.addError("tagName", "name of the tag cannot contain a :");
        }
        else if(result.getTagName().contains("~")){
            result.addError("tagName", "name of the tag cannot contain a ~");
        }
        else if(result.getTagName().contains("^")){
            result.addError("tagName", "name of the tag cannot contain a ^");
        }
        else
        if (tagNameExists(result)) {
            result.addError("tagName", "this name is already used for a tag");
        }


    }

    private boolean tagNameExists(CreateTagFormResult result) {
        Repository repository = repositoryService.getById(result.getRepositoryId());
        Command<Boolean> command = customCommandFactory.tagByName(repository, result.getTagName());
        return command.call();
    }
}
