package be.contribute.atlassian.validator;


import be.contribute.atlassian.tag.rest.UpdateTagRestResource;

public interface TagRestValidator {
    RestValidationResult validate(UpdateTagRestResource updateTagRestResource);
}
