package be.contribute.atlassian.exception;


public class TagNameException extends RuntimeException {
    private String errorMessage;

    public TagNameException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
