package be.contribute.atlassian.web;


public class ChangesetViewable {
    private String shortCommitHash;
    private String commitUrl;
    private String commitDate;
    private String avatarUrl;
    private String commitHash;
    private String name;
    private String message;

    public ChangesetViewable() {
    }

    public String getShortCommitHash() {
        return shortCommitHash;
    }

    public void setShortCommitHash(String shortCommitHash) {
        this.shortCommitHash = shortCommitHash;
    }

    public String getCommitUrl() {
        return commitUrl;
    }

    public void setCommitUrl(String commitUrl) {
        this.commitUrl = commitUrl;
    }

    public String getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(String commitDate) {
        this.commitDate = commitDate;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCommitHash() {
        return commitHash;
    }

    public void setCommitHash(String commitHash) {
        this.commitHash = commitHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
