package be.contribute.atlassian.command;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class TagListCommandOutputHandler implements CommandOutputHandler<List<String>> {

    private Watchdog watchdog;
    private List<String> lines = new ArrayList<String>();

    @Nullable
    @Override
    public List<String> getOutput() {
        return lines;
    }

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        BufferedReader in = getBufferedReader(inputStream);
        try {
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
           // e.printStackTrace();
        }
    }

    @Override
    public void complete() throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }

    protected BufferedReader getBufferedReader(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream));
    }
}
