package be.contribute.atlassian.command;


import be.contribute.atlassian.DetailedTag;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.Command;

import java.util.List;

public interface CustomCommandFactory {

    Command<Void> tag(Repository repository, String commitId, String tagName, String tagDescription);
    Command<List<String>> tagsForCommit(Repository repository, String commitId);
    Command<Boolean> tagByName(Repository repository, String tagName);
    Command<DetailedTag> tagInformation( Repository repository, String tagName);
}
