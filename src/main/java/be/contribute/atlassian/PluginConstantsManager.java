package be.contribute.atlassian;


public class PluginConstantsManager {
    public static String getGitDateFormat() {
        return "MMM dd HH:mm:ss YYYY Z";
    }

    public static final String START_PGP = "-----BEGIN PGP SIGNATURE-----";
    public static final String END_PGP = "-----END PGP SIGNATURE-----";
}
