package be.contribute.atlassian.event;

import com.atlassian.bitbucket.event.repository.AbstractRepositoryRefsChangedEvent;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;

import javax.annotation.Nonnull;
import java.util.Collections;


public class TagCreatedEvent extends AbstractRepositoryRefsChangedEvent {

    public TagCreatedEvent(@Nonnull Object source, @Nonnull Repository repository, @Nonnull RefChange refChange) {
        super(source, repository, Collections.<RefChange>singleton(refChange));
    }
}
