package be.contribute.atlassian.servlet;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.service.RepositoryResolver;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactory;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class ViewTagServlet extends AbstractTagServlet {

    private static final Logger log = LoggerFactory.getLogger(ViewTagServlet.class);
    private static final int TAGS_PER_PAGE_LIMIT = 5;
    private final TagService tagService;
    private final PageBuilderService pageBuilderService;
    private final TagRestResourceTransformationFactory tagRestResourceTransformationFactory;
    private final PermissionService permissionService;

    public ViewTagServlet(SoyTemplateRenderer soyTemplateRenderer, RepositoryService repositoryService, RepositoryResolver resolver, TagService tagService, PageBuilderService pageBuilderService, TagRestResourceTransformationFactory tagRestResourceTransformationFactory, ApplicationPropertiesService applicationPropertiesService, PermissionService permissionService) {
        super(soyTemplateRenderer, repositoryService, resolver, applicationPropertiesService);
        this.tagService = tagService;
        this.pageBuilderService = pageBuilderService;
        this.tagRestResourceTransformationFactory = tagRestResourceTransformationFactory;
        this.permissionService = permissionService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        String repoSlug = req.getParameter("repo");
        String projectKey = req.getParameter("projKey");
        Repository repository = repositoryResolver.resolveRepository(projectKey, repoSlug, Boolean.parseBoolean(req.getParameter("personal")));

        int startingFrom = getStartingFrom(req);

        paramsMap.put("repository", repository);
        paramsMap.put("startingFrom", startingFrom);
        paramsMap.put("baseUrl", getBaseUrl().toString());
        PageRequestImpl pageRequest = new PageRequestImpl(startingFrom, TAGS_PER_PAGE_LIMIT);
        Page<DetailedTag> tags = tagService.getTags(repository, pageRequest);
        log.trace("We found " + tagService.getTags(repository, pageRequest).getSize() + " tags.");

        Page<DetailedTagRestResource> detailedTagRestResourcePage = tags.transform(tagRestResourceTransformationFactory.getTransformation(repository));
        paramsMap.put("tags", detailedTagRestResourcePage);
        paramsMap.put("permission", permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE));

        pageBuilderService.assembler().resources().requireContext("contribute.page.taglist");
        render(resp, "Contribute.Stash.Templates.TagList.tagOverview", paramsMap);
    }


    private int getStartingFrom(HttpServletRequest req) {
        String param = req.getParameter("startingFrom");
        return param != null ? Integer.parseInt(param) : 0;
    }

    @Override
    public String getResource() {
        return "be.contribute.atlassian.stash-tagging-support:tag-list-resources";
    }
}
