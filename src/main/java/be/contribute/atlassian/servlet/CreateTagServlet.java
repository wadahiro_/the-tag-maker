package be.contribute.atlassian.servlet;

import be.contribute.atlassian.service.RepositoryResolver;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.service.ViewableManager;
import be.contribute.atlassian.validator.CreateTagFormValidator;
import be.contribute.atlassian.web.ChangesetViewable;
import be.contribute.atlassian.web.CreateTagFormResult;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


public class CreateTagServlet extends AbstractTagServlet {

    private static final Logger log = LoggerFactory.getLogger(CreateTagServlet.class);
    private static final String CREATE_VIEW = "Contribute.Stash.Templates.CreateTag.createTagView";
    private static final String NOT_ALLOWED_MSG = "Sorry mate. You're not allowed to create tags in this repository";

    private CommitService commitService;
    private CreateTagFormValidator createTagFormValidator;
    private final ViewableManager viewableManager;
    private final TagService tagService;
    private final PermissionService permissionService;


    public CreateTagServlet(SoyTemplateRenderer soyTemplateRenderer, RepositoryService repositoryService, RepositoryResolver resolver, CommitService commitService, CreateTagFormValidator createTagFormValidator, ApplicationPropertiesService applicationPropertiesService, ViewableManager viewableManager, TagService tagService, PermissionService permissionService) {
        super(soyTemplateRenderer, repositoryService, resolver, applicationPropertiesService);
        this.commitService = commitService;
        this.createTagFormValidator = createTagFormValidator;
        this.viewableManager = viewableManager;
        this.tagService = tagService;
        this.permissionService = permissionService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String, Object> params = new HashMap<String, Object>();

        String commitId = req.getParameter("commitId");
        Repository repository = getRepository(req);
        if (!permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)) {
            resp.sendError(401,NOT_ALLOWED_MSG);
            return;
        }

        Commit commit = getChangeset(commitId, repository);
        params.put("changesetViewable", getChangesetViewable(commit, repository));
        params.put("repository", repository);
        params.put("lastPage", getLastPage(req, repository));
        params.put("formResult", new CreateTagFormResult());
        render(resp, CREATE_VIEW, params);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CreateTagFormResult form = new CreateTagFormResult(req);

        Repository repository = repositoryService.getById(form.getRepositoryId());
        if (!permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)) {
            resp.sendError(401,NOT_ALLOWED_MSG);
            return;
        }

        createTagFormValidator.validate(form);

        if (!form.hasErrors()) {

            tagService.createTag(form.getTagName(), form.getDescription(), form.getCommitId(), repository);

            resp.sendRedirect(getLastPage(req, repository));

        } else {
            redirectToCreateScreen(req, resp, form, repository);
        }


    }

    private void redirectToCreateScreen(HttpServletRequest req, HttpServletResponse resp, CreateTagFormResult form, Repository repository) throws IOException, ServletException {
        HashMap<String, Object> params = new HashMap<String, Object>();
        Commit changeset = getChangeset(form.getCommitId(), repository);
        params.put("changesetViewable", getChangesetViewable(changeset, repository));
        params.put("repository", repository);
        params.put("formResult", form);
        params.put("lastPage", getLastPage(req, repository));
        render(resp, CREATE_VIEW, params);
    }

    @Override
    public String getResource() {
        return "be.contribute.atlassian.stash-tagging-support:create-tag-resources";
    }



    private String getLastPage(HttpServletRequest req, Repository repository) {
        String lastPage = resolveLastPage(req);
        if (!lastPage.toLowerCase().startsWith(getBaseUrl().toString().toLowerCase())) {
            return getBaseUrl() + "/plugins/servlet/view-tags?projKey=" + repository.getProject().getKey() + "&repo=" + repository.getSlug();
        }
        return lastPage;
    }

    private Commit getChangeset(String commitId, Repository repository) {
        CommitRequest request = new CommitRequest.Builder(repository, commitId).build();
        return commitService.getCommit(request);
    }

    private ChangesetViewable getChangesetViewable(Commit changeset, Repository repository) {
        return viewableManager.createViewAble(changeset, repository);
    }

    private String resolveLastPage(HttpServletRequest req) {
        String lastPage = req.getParameter("lastPage");
        if (StringUtils.isBlank(lastPage)) {
            return StringUtils.defaultString(req.getHeader("Referer"));
        }
        return lastPage;
    }


    private Repository getRepository(HttpServletRequest req) {
        String repoIdParam = req.getParameter("repoId");
        if (StringUtils.isNotBlank(repoIdParam)) {
            int repoId = Integer.parseInt(repoIdParam);
            return repositoryService.getById(repoId);
        }
        return repositoryResolver.resolveRepository(req.getParameter("projectKey"), req.getParameter("repoSlug"), Boolean.parseBoolean(req.getParameter("personal")));
    }
}
