package be.contribute.atlassian.service;

import be.contribute.atlassian.web.ChangesetViewable;
import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.util.DateFormatter;

import java.util.Date;



public class ViewableManagerImpl implements ViewableManager {

    private static final int MIN_SIZE = 32;
    private AvatarService avatarService;
    private CommitService commitService;
    private NavBuilder navBuilder;
    private DateFormatter dateFormatter;

    public ViewableManagerImpl(AvatarService avatarService, CommitService commitService, NavBuilder navBuilder, DateFormatter dateFormatter) {
        this.avatarService = avatarService;
        this.commitService = commitService;
        this.navBuilder = navBuilder;
        this.dateFormatter = dateFormatter;
    }

    @Override
    public ChangesetViewable createViewAble(Commit commit, Repository repository) {
        ChangesetViewable changesetViewable = new ChangesetViewable();
        changesetViewable.setCommitUrl(getCommitURL(repository, commit.getId()));
        changesetViewable.setCommitDate(formatDate(commit.getAuthorTimestamp()));
        changesetViewable.setCommitHash(commit.getId());
        changesetViewable.setShortCommitHash(commit.getDisplayId());
        changesetViewable.setName(commit.getAuthor().getName());
        changesetViewable.setAvatarUrl(avatarService.getUrlForPerson(commit.getAuthor(), new AvatarRequest(false, MIN_SIZE)));
        changesetViewable.setMessage(commit.getMessage());
        return changesetViewable;
    }

    private String formatDate(Date date) {
        return dateFormatter.formatDate(date, DateFormatter.FormatType.LONG);
    }

    private String getCommitURL(Repository repository, String commitHash) {
        return navBuilder.project(repository.getProject()).repo(repository).commit(commitHash).buildAbsolute();
    }
}
