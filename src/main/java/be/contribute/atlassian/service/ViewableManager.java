package be.contribute.atlassian.service;


import be.contribute.atlassian.web.ChangesetViewable;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.repository.Repository;

public interface ViewableManager {
    public ChangesetViewable createViewAble(Commit changeset, Repository repository);
}
