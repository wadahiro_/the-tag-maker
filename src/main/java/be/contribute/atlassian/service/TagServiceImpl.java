package be.contribute.atlassian.service;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.command.CustomCommandFactory;
import be.contribute.atlassian.event.TagCreatedEvent;
import be.contribute.atlassian.event.TagDeletedEvent;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.scm.*;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitRefCommandFactory;
import com.atlassian.bitbucket.scm.git.ref.GitCreateTagCommandParameters;
import com.atlassian.bitbucket.scm.git.ref.GitDeleteTagCommandParameters;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.bitbucket.scm.Command;
import com.atlassian.bitbucket.scm.ref.CreateTagCommandParameters;
import com.atlassian.bitbucket.scm.TagsCommandParameters;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageImpl;
import com.atlassian.bitbucket.util.PageRequest;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class TagServiceImpl implements TagService {

    private static final String EMPTY_HASH = Strings.repeat("0", 40);


    private CustomCommandFactory customCommandFactory;
    private ScmService scmService;
    private final GitScm gitScm;
    private final EventPublisher eventPublisher;
    private final GitRefCommandFactory gitRefCommandFactory;
    private static final Logger log = LoggerFactory.getLogger(TagServiceImpl.class);

    public TagServiceImpl(CustomCommandFactory customCommandFactory, ScmService scmService, EventPublisher eventPublisher, GitRefCommandFactory gitRefCommandFactory, GitScm gitScm) {
        this.customCommandFactory = customCommandFactory;
        this.scmService = scmService;
        this.gitScm = gitScm;
        this.eventPublisher = eventPublisher;
        this.gitRefCommandFactory = gitRefCommandFactory;
    }

    @Override
    public Page<DetailedTag> getTags(Repository repository, PageRequest pageRequest) {
        ArrayList<DetailedTag> detailedTags = new ArrayList<DetailedTag>();

        TagsCommandParameters.Builder builder = new TagsCommandParameters.Builder();
        TagsCommandParameters build = builder.build();
        ScmCommandFactory commandFactory = scmService.getCommandFactory(repository);
        Command<Page<Tag>> tagsCommand = commandFactory.tags(build, pageRequest);

        //TODO: refactor this later to use google Guava LoadingChache
        Page<Tag> call = tagsCommand.call();
        Iterable<Tag> tags = call.getValues();

        log.debug("We found " + call.getSize() + " tags.");

        for (Tag tag : tags) {
            log.debug("Tag - commitHash: " + tag.getHash() + "\n");
            log.debug("Tag - displayId: " + tag.getDisplayId() + "\n");
            log.debug("Tag - id: " + tag.getId() + "\n");
            log.debug("Tag - latestCommit: " + tag.getLatestCommit() + "\n");
            log.debug("Tag - type: " + tag.getType().toString() + "\n");
            Command<DetailedTag> detailedTagCommand = customCommandFactory.tagInformation(repository, tag.getDisplayId());
            log.debug("Running command: " + detailedTagCommand.toString());
            DetailedTag detailedTag = detailedTagCommand.call();
            detailedTags.add(detailedTag);
        }
        return new PageImpl<DetailedTag>(pageRequest,detailedTags, call.getIsLastPage());
    }

    @Override
    public void createTag(String name, String message, String commitHash, Repository repository) {
        log.debug("Trying to create a tag with name " + name + ", message '" + message + "' on commitHash " + commitHash + " in repo " + repository.getName());
        CreateTagCommandParameters.Builder builder = new CreateTagCommandParameters.Builder();
        CreateTagCommandParameters parameters = builder
                .message(message)
                .startPoint(commitHash)
                .name(name)
                .build();
        GitCommand<Tag> command = gitRefCommandFactory.createTag(repository, parameters);
        Tag tag = command.call();

        raiseTagCreatedEvent(repository, tag);
    }

    private void raiseTagCreatedEvent(Repository repository, Tag tag) {
        log.debug("Trying to raise a Tag Created Event for repository " + repository.getName() + " with tag " + tag.getHash());
        log.debug("The Tag we received: ");
        log.debug("Hash: " + tag.getHash());
        log.debug("Display id: " + tag.getDisplayId());
        log.debug("Id: " + tag.getId());
        log.debug("Latest Commit: " + tag.getLatestCommit());
        log.debug("Tag Type: " + tag.getType().toString());
        MinimalRef ref = new SimpleMinimalRef.Builder()
                .id(tag.getHash())
                .displayId(tag.getDisplayId())
                .type(tag.getType())
                .build();
        SimpleRefChange.Builder refChangeBuilder = new SimpleRefChange.Builder()
                .fromHash(tag.getHash())
                .ref(ref)
                .type(RefChangeType.ADD)
                .toHash(EMPTY_HASH);
        log.debug("SimpleRefChangeBuilder built");

        SimpleRefChange refChange = refChangeBuilder.build();
        log.debug("RefChange built");

        TagCreatedEvent tagCreatedEvent = new TagCreatedEvent(this, repository, refChange);
        log.debug("TagCreatedEvent created");
        eventPublisher.publish(tagCreatedEvent);
        log.debug("Event published");
    }

    @Override
    public void deleteTag(String name, Repository repository) {
        Command<DetailedTag> originalTagCommand = customCommandFactory.tagInformation(repository, name);
        DetailedTag originalTag = originalTagCommand.call();

        GitDeleteTagCommandParameters parameters = new GitDeleteTagCommandParameters.Builder()
                .name(name)
                .build();
        GitCommand<Void> deleteCommand = gitRefCommandFactory.deleteTag(repository, parameters);
        deleteCommand.call();

        MinimalRef ref = new SimpleTag.Builder()
                .displayId(originalTag.getCommitHash())
                .id(originalTag.getResolvedTagId())
                .hash(originalTag.getCommitHash())
                .latestCommit(originalTag.getCommitHash())
                .build();
//        MinimalRef ref = new SimpleMinimalRef.Builder()
//                .id(originalTag.getResolvedTagId())
//                .type(new SimpleTag.Builder().build().getType())
//                .displayId(name)
//                .build();
        SimpleRefChange.Builder refChangeBuilder = new SimpleRefChange.Builder();
        refChangeBuilder
                .fromHash(originalTag.getCommitHash())
                .ref(ref)
                .toHash(EMPTY_HASH)
                .type(RefChangeType.DELETE);

        TagDeletedEvent tagDeletedEvent = new TagDeletedEvent(this, repository, refChangeBuilder.build());
        eventPublisher.publish(tagDeletedEvent);
    }

    @Override
    public Tag updateTag(String name, String message, String commitHash, Repository repository) {
        log.debug("Trying to perform the update of the tag " + name);
        GitRefCommandFactory refCommandFactory = gitScm.getRefCommandFactory();
        GitCreateTagCommandParameters parameters = new GitCreateTagCommandParameters.Builder()
                .force(true)
                .message(message)
                .name(name)
                .startPoint(commitHash)
                .build();
        log.debug("Created the parameters.");
        GitCommand<Tag> command = gitRefCommandFactory.createTag(repository, parameters);
        log.debug("Created the command.");
        Tag tag = command.call();
        log.debug("Executed the command");
        raiseTagCreatedEvent(repository, tag);
        log.debug("Event raised");
        return tag;
    }

    @Override
    public void renameTag(String originalTagName, String newTagName, String message, String commitHash, Repository repository) {
        deleteTag(originalTagName, repository);
        createTag(newTagName, message, commitHash, repository);
    }

    @Override
    public DetailedTag getTag(String name, Repository repository) {
        Command<DetailedTag> detailedTagCommand = customCommandFactory.tagInformation(repository, name);
        return detailedTagCommand.call();
    }


}
