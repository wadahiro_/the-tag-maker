package be.contribute.atlassian;


import be.contribute.atlassian.enums.TagType;
import org.joda.time.DateTime;

import static be.contribute.atlassian.util.PluginArrayUtil.append;

public class DetailedTag {
    private String tagName;
    private Tagger tagger;
    private String[] description = new String[]{};
    private String commitHash;
    private DateTime taggedDateTime = null;
    private String taggedTagName;
    private String signature = "";
    private String signatureVersion;
    private TagType tagType;

    public DetailedTag() {
    }

    public DateTime getTaggedDateTime() {
        return taggedDateTime;
    }

    public void setTaggedDateTime(DateTime taggedDateTime) {
        this.taggedDateTime = taggedDateTime;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Tagger getTagger() {
        return tagger;
    }

    public void setTagger(Tagger tagger) {
        this.tagger = tagger;
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    public void addDescriptionLine(String line) {
        description = append(description, line);
    }

    public String getCommitHash() {
        return commitHash;
    }

    public void setCommitHash(String commitHash) {
        this.commitHash = commitHash;
    }

    public String getTaggedTagName() {
        return taggedTagName;
    }

    public void setTaggedTagName(String taggedTagName) {
        this.taggedTagName = taggedTagName;
    }

    public String getPointsTo() {
        return taggedTagName != null ? taggedTagName : commitHash;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSignatureVersion() {
        return signatureVersion;
    }

    public void setSignatureVersion(String signatureVersion) {
        this.signatureVersion = signatureVersion;
    }

    public TagType getTagType() {
        return tagType;
    }

    public void setTagType(TagType tagType) {
        this.tagType = tagType;
    }

    public String getResolvedTagId()  {
        return "refs/tags/" +tagName;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TagName: " + tagName + "\n");
        if (tagger != null) {
            if (tagger.getName() != null && tagger.getEmail() != null) {
                sb.append("Tagger: " + tagger.getName() + " (" + tagger.getEmail() + ")\n");
            }
        }
        sb.append("Description: \n");
        for (String desc : description) {
            sb.append(desc + "\n");
        }
        sb.append("CommitHash: " + commitHash + "\n");
        if (taggedDateTime != null) {
            sb.append("TaggetDateTime: " + taggedDateTime.toString() + "\n");
        }
        sb.append("TaggedTagName: " + taggedTagName + "\n");
        sb.append("Signature: " + signature  + "\n");
        sb.append("SignatureVersion: " + signatureVersion  + "\n");
        sb.append("TagType: " + tagType.name());
        return sb.toString();
    }
}
