package be.contribute.atlassian.condition;

import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.web.conditions.AbstractPermissionCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class CanTagCondition extends AbstractPermissionCondition {
    private final RepositoryService repositoryService;

    private static final Logger log = LoggerFactory.getLogger(CanTagCondition.class);

    public CanTagCondition(PermissionService permissionService, RepositoryService repositoryService) {
        super(permissionService, false);
        this.repositoryService = repositoryService;
    }



    @Override
    protected boolean hasPermission(Map<String, Object> context) {
        ServletRequest request = (ServletRequest) context.get("request");
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String path = httpServletRequest.getServletPath();
        Repository repository = extractRepository(path);
        if (repository != null) {
            return permissionService.hasRepositoryPermission(repository, getPermission());
        }
        return false;
    }


    private Repository extractRepository(String pathInfo) {
        String[] strings = pathInfo.split("/");
        String projectKey = null;
        String reposSlug = null;
        for (int i = 0; i < strings.length; i++) {
            String currentPath = strings[i];
            if (i != 0) {
                String lastPath = strings[i - 1];
                if ("projects".equals(lastPath) && currentPath != null) {
                    projectKey = strings[i];
                }
                if ("users".equals(lastPath) && currentPath != null) {
                    projectKey = "~" + strings[i];
                }
                if ("repos".equals(lastPath) && currentPath != null) {
                    reposSlug = strings[i];
                }
            }
        }
        return getRepository(projectKey, reposSlug);
    }

    private Repository getRepository(String projectKey, String repoSlug) {
        log.debug("Fetching repository with projectKey " + projectKey + " and repoSlug " + repoSlug);
        Repository repository = null;
        try {
            repository = repositoryService.getBySlug(projectKey, repoSlug);
        } catch (Exception e) {
            log.error("error while fetching repository", e);
        }
        return repository;
    }
}
