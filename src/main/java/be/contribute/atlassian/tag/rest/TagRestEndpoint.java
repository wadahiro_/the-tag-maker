package be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.validator.RestValidationResult;
import be.contribute.atlassian.validator.TagRestValidator;
import com.atlassian.bitbucket.repository.InvalidAuthorException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.rest.util.RestPage;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tag")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class TagRestEndpoint {


    private final TagService tagService;
    private final RepositoryService repositoryService;
    private final TagRestValidator tagRestValidator;
    private final TagRestResourceTransformationFactory tagRestResourceTransformationFactory;
    private final PermissionService permissionService;
    private final static Logger log = LoggerFactory.getLogger(TagRestEndpoint.class);

    public TagRestEndpoint(TagService tagService, RepositoryService repositoryService, TagRestValidator tagRestValidator, TagRestResourceTransformationFactory tagRestResourceTransformationFactory, PermissionService permissionService) {
        this.tagService = tagService;
        this.repositoryService = repositoryService;
        this.tagRestValidator = tagRestValidator;
        this.tagRestResourceTransformationFactory = tagRestResourceTransformationFactory;
        this.permissionService = permissionService;
    }

    @DELETE
    @Path("project/{projectKey}/rep/{repoSlug}/tag/{tagName}")
    public Response delete(@PathParam("projectKey") final String projectKey, @PathParam("repoSlug") final String repoSlug, @PathParam("tagName") final String tagName) {
        Repository repository = repositoryService.getBySlug(projectKey, repoSlug);
        if (repository == null) {
            log.warn("We weren't able to find a repository with the given credentials.");
        }

        if (editNotAllowed(repository)){
            return Response.status(401).build();
        }

        try {
            tagService.deleteTag(tagName, repository);
        } catch (InvalidAuthorException e) {
            log.error("Oops", e);
            return Response.status(401).build();
        } catch (Exception e) {
            log.error("Oops", e);
            return Response.status(500).build();
        }

        return Response.ok().build();
    }

    @PUT
    @Path("update")
    public Response update(UpdateTagRestResource tagRestResource) {
        RestValidationResult result = tagRestValidator.validate(tagRestResource);
        if (!result.isValid()) {
            String jsonErrors = new Gson().toJson(result.getErrors());
            return Response.status(400).entity(jsonErrors).build();
        }

        Repository repository = repositoryService.getBySlug(tagRestResource.getProjectKey(), tagRestResource.getRepoSlug());

        if (editNotAllowed(repository)){
            return Response.status(401).build();
        }

        Tag tag = tagService.updateTag(tagRestResource.getName(), tagRestResource.getMessage(), tagRestResource.getCommitId(), repository);
        DetailedTag detailedTag = tagService.getTag(tag.getDisplayId(), repository);
        DetailedTagRestTransformFunction transformation = tagRestResourceTransformationFactory.getTransformation(repository);
        DetailedTagRestResource detailedTagRestResource = transformation.apply(detailedTag);
        String json = new Gson().toJson(detailedTagRestResource);
        return Response.ok(json).build();
    }

    private boolean editNotAllowed(Repository repository) {
        return !permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE);
    }

    @GET
    @Path("project/{projectKey}/rep/{repoSlug}/tag/{tagName}")
    public Response getTag(@PathParam("projectKey") final String projectKey, @PathParam("repoSlug") final String repoSlug, @PathParam("tagName") final String tagName) {
        Repository repository = repositoryService.getBySlug(projectKey, repoSlug);
        DetailedTag tag = tagService.getTag(tagName, repository);

        if (tag.getTagName() != null) {
            DetailedTagRestTransformFunction transformation = tagRestResourceTransformationFactory.getTransformation(repository);
            DetailedTagRestResource detailedTagRestResource =transformation.apply(tag);
            return Response.ok(detailedTagRestResource).build();
        }   else {
            return Response.status(400).build();
        }

    }

    @GET
    @Path("project/{projectKey}/rep/{repoSlug}/tags")
    public Response getTags(@PathParam("projectKey") final String projectKey, @PathParam("repoSlug") final String repoSlug, @Context PageRequest pageRequest) {
        Repository repository = repositoryService.getBySlug(projectKey, repoSlug);
        Page<DetailedTag> tags = tagService.getTags(repository, pageRequest);
        DetailedTagRestTransformFunction transformation = tagRestResourceTransformationFactory.getTransformation(repository);
        Page<DetailedTagRestResource> resourcePage = tags.transform(transformation);
        return Response.ok(new RestPage<DetailedTagRestResource>(resourcePage)).build();
    }
}
