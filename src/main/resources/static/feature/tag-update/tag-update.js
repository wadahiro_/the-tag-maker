define('tag-maker/feature/update-tag', [
    'aui',
    'jquery',
    'lodash',
    'bitbucket/util/navbuilder',
    'bitbucket/util/events',
    'exports',
    'aui/form-notification'
], function (AJS, $, _, nav, events, exports, notification) {

    var updateDialog = null;

    function createUpdateDialog() {
        $(".update-tag").live("click", function (e) {
            e.preventDefault();
            if (updateDialog !== null) {
                updateDialog.remove();
                updateDialog = null;
            }
            var tagName = getTagData(this, 'tag-name');
            showDialog(tagName, this);
        });

        $('#update-tag-dialog-form').live('submit', function (e) {
            e.preventDefault();
        });

        $("#dialog-close-button").live('click', function (e) {
            e.preventDefault();
            if (updateDialog == null) {
                updateDialog = $('#update-tag-dialog');
                updateDialog.hide();
                updateDialog = null;
            } else {
                updateDialog.remove();
                updateDialog = null;
            }
        });
    }

    function getTagData(trigger, dataKey) {
        var attr = 'data-' + dataKey;
        return $(trigger).closest('[' + attr + ']').attr(attr);
    }

    function showDialog(tagName, trigger) {
        $.ajax({
            url: getTagRestUrl(tagName),
            type: 'GET',
            statusCode: {
                '400': false,
                '403': false,
                '409': false
            }
        })
            .done(function (tagData) {
                updateDialog = AJS.dialog2(Contribute.Stash.Templates.UpdateTag.dialog(tagData));
                updateDialog.show();
            });

        $('.update-tag-btn').live('click', function (e) {
            e.preventDefault();
            var dataToSubmit = {
                projectKey: getProjectKey(),
                repoSlug: getRepo(),
                name: tagName,
                message: $("#message-input").val(),
                commitId: $("#commitId-input").val()
            };

            $.ajax({
                url: getTagUpdateRestUrl(),
                type: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(dataToSubmit),
                statusCode: {
                    '400': function (xhr, textStatus, errorThrown, errors, dominantError) {
                        //$('.error-messages').removeClass("hidden");
                        for (var i = 0; i < errors.length; i++) {
                            var error = errors[i];
                            $('#' + error.context + '-input').attr('data-aui-notification-error', error.errorMessage);
                        }
                        return false;
                    },
                    '403': false,
                    '500': function (xhr, textStatus, errorThrown, errors, dominantError) {
                        updateDialog.remove();
                        return $.extend({}, dominantError, {
                            title: AJS.I18n.getText('tag-maker.web.update-tag.error.500.title'),
                            fallbackUrl: false,
                            shouldReload: true
                        });
                    }
                }
            })
                .done(function (data) {
                    var tag = {
                        tag: data,
                        permission: true
                    };
                    console.log(tag);
                    var tagRow = Contribute.Stash.Templates.TagList.tagCells(tag);
                    $(trigger).closest('tr').html(tagRow);
                    updateDialog.remove();
                    updateDialog = null;
                })

        });
    }

    function getProjectKey() {
        var attr = 'data-projectkey';
        return $('#content').attr(attr);
    }

    function getRepo() {
        var attr = 'data-reposlug';
        return $('#content').attr(attr);
    }

    function getTagRestUrl(tagName) {
        return nav.newBuilder()
            .addPathComponents("rest", "tag-maker", "1.0", "tag", "project", getProjectKey(), "rep", getRepo(), "tag", tagName)
            .buildAbsolute();
    }

    function getTagUpdateRestUrl() {
        return nav.newBuilder()
            .addPathComponents("rest", "tag-maker", "1.0", "tag", "update")
            .buildAbsolute();
    }

    exports.onReady = function () {
        createUpdateDialog();
    };
})
;

jQuery(document).ready(function () {
    require('tag-maker/feature/update-tag').onReady();
});