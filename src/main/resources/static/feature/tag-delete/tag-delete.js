define('tag-maker/feature/delete-tag', [
    'aui',
    'jquery',
    'lodash',
    'bitbucket/util/navbuilder',
    'bitbucket/util/events',
    'exports'
], function (AJS, $, _, nav, events, exports) {

    var notificationsAnchor = '.branch-list-panel .notifications',
        tableBodySelector = '.tag-list-rows';

    function createConfirmationDialog() {
        var tagName = getTagData(this, 'tag-name');
        var dialog = new AJS.Dialog({
            width: 400,
            height: 200,
            id: "delete-tag-dialog",
            closeOnOutsideClick: false
        });
        dialog.addHeader(AJS.I18n.getText('bitbucket.page.tag.deletion.title'), "warning-header");
        dialog.addPanel("Confirmation", "<p>" + AJS.I18n.getText('bitbucket.page.tag.deletion.body', '<b class="tag-name">' + tagName + '</b>') + "</p>", 'panel-body');
        dialog.addButton(AJS.I18n.getText('bitbucket.web.button.delete'), function (promise, $trigger, removeDialog, dialog, $spinner) {
            //var tagName = getTagData($trigger, 'tag-name');
            //AJS.spinner.show();
            deleteTagByRest(branchName).done(function () {
                /*events.trigger('bitbucket.page.branches.revisionRefRemoved', null, {
                 id: branchId,
                 displayId: branchDisplayId,
                 latestChangeset: latestChangeset
                 });*/

                deleteTagRow(tagName);



            }).fail(function (xhr, textStatus, errorThrown, data) {
                addErrorNotifications(tagName, data && data.errors);
            }).always(removeDialog);
        }, "confirm-button");
        dialog.addLink(AJS.I18n.getText('bitbucket.web.button.cancel'), function (dialog) {
            dialog.hide();
        });
    }

    function deleteTagRow(tagName){
        var tableBody = $(tableBodySelector);
        var row = tableBody.find('tr[data-tag-name="' + tagName + '"]');
        row.remove();
    }

    function getTagData(trigger, dataKey) {
        var attr = 'data-' + dataKey;
        return $(trigger).closest('[' + attr + ']').attr(attr);
    }

    function getProjectKey() {
        var attr = 'data-projectkey';
        return $('#content').attr(attr);
    }

    function getRepo() {
        var attr = 'data-reposlug';
        return $('#content').attr(attr);
    }

    function deleteTagByRest(object, tagName) {
        return $.ajax({
            url: getTagRestUrl(object, tagName),
            type: 'DELETE',
            statusCode: {
                '400': false,
                '403': false,
                '409': false
            }
        });
    }

    function getTagRestUrl(object, tagName) {
        return nav.newBuilder()
            .addPathComponents("rest", "tag-maker", "1.0", "tag", "project", getProjectKey(), "rep", getRepo(), "tag", tagName)
            .buildAbsolute();
    }

    function addErrorNotifications(tagName, errors) {
        $(notificationsAnchor).empty();
        _.forEach(errors, function (error) {
            AJS.messages.error(notificationsAnchor, {
                body: AJS.escapeHtml(AJS.I18n.getText('bitbucket.feature.branch-deletion.failure',
                    tagName, error && error.message)),
                closeable: true
            });
        });
    }

    exports.onReady = function () {
        createConfirmationDialog();
    };

    $(".delete-tag").click(function(e) {
        var tagName = getTagData(this, 'tag-name');
        var dialog = new AJS.Dialog({
            width: 400,
            height: 200,
            id: "delete-tag-dialog",
            closeOnOutsideClick: false
        });
        dialog.addHeader(AJS.I18n.getText('bitbucket.page.tag.deletion.title'), "warning-header");
        dialog.addPanel("Confirmation", "<p>" + AJS.I18n.getText('bitbucket.page.tag.deletion.body', '<b class="tag-name">' + tagName + '</b>') + "</p>", 'panel-body');
        dialog.addButton(AJS.I18n.getText('bitbucket.web.button.delete'), function (dialog) {
                    //var tagName = getTagData($trigger, 'tag-name');
                    //AJS.spinner.show();
                    deleteTagByRest(this, tagName).done(function () {
                        /*events.trigger('bitbucket.page.branches.revisionRefRemoved', null, {
                         id: branchId,
                         displayId: branchDisplayId,
                         latestChangeset: latestChangeset
                         });*/

                        deleteTagRow(tagName);
                        dialog.hide();
                    }).fail(function (xhr, textStatus, errorThrown, data) {
                        addErrorNotifications(tagName, data && data.errors);
                        dialog.hide();
                    });
                }, "confirm-button");
        dialog.addLink(AJS.I18n.getText('bitbucket.web.button.cancel'), function (dialog) {
            dialog.hide();
        });
        dialog.show();
    });
});

jQuery(document).ready(function () {
    require('tag-maker/feature/delete-tag').onReady();
});